<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "Admin" middleware group. Enjoy building your Admin!
|
*/


/*
|--------------------------------------------------------------------------
| Admin Auth
|--------------------------------------------------------------------------
| Here is where admin auth routes exists for login and log out
*/
Route::group([
    'namespace'  => 'Auth',
], function() {
    Route::get('login', ['uses' => 'LoginController@showLoginForm','as'=>'admin.login']);
    Route::post('login', ['uses' => 'LoginController@login']);
    Route::group([
        'middleware' => 'App\Http\Middleware\RedirectIfAuthenticatedAdmin',
    ], function() {
        Route::post('logout', ['uses' => 'LoginController@logout','as'=>'admin.logout']);
    });
});
/*
|--------------------------------------------------------------------------
| Admin After login in
|--------------------------------------------------------------------------
| Here is where admin panel routes exists after login in
*/
Route::group([
    'middleware'  => ['App\Http\Middleware\RedirectIfAuthenticatedAdmin'],
], function() {
    Route::get('/', 'HomeController@index');
    Route::post('notification/send', 'HomeController@general_notification');

    /*
    |--------------------------------------------------------------------------
    | Admin > App Management
    |--------------------------------------------------------------------------
    | Here is where App Management routes
    */

    Route::group([
        'prefix'=>'app_managements',
        'namespace'=>'AppManagement',
    ],function () {
        Route::group([
            'prefix'=>'admins'
        ],function () {
            Route::get('/','AdminController@index');
            Route::get('/create','AdminController@create');
            Route::post('/','AdminController@store');
            Route::get('/{admin}','AdminController@show');
            Route::get('/{admin}/edit','AdminController@edit');
            Route::put('/{admin}','AdminController@update');
            Route::delete('/{admin}','AdminController@destroy');
            Route::patch('/update/password',  'AdminController@updatePassword');
            Route::get('/option/export','AdminController@export');
            Route::get('/{id}/activation','AdminController@activation');
        });
        Route::group([
            'prefix'=>'logs'
        ],function () {
            Route::get('/','LogController@index');
            Route::get('/option/export','LogController@export');
        });
    });
    /*
    |--------------------------------------------------------------------------
    | Category > App Setting
    |--------------------------------------------------------------------------
    | Here is where App Setting routes
    */

    Route::group([
        'prefix'=>'app_settings',
        'namespace'=>'AppSetting',
    ],function () {
        Route::group([
            'prefix'=>'settings'
        ],function () {
            Route::get('/','SettingController@index');
            Route::get('/{admin}/edit','SettingController@edit');
            Route::put('/{admin}','SettingController@update');
        });
        Route::group([
            'prefix'=>'categories'
        ],function () {
            Route::get('/','CategoryController@index');
            Route::get('/create','CategoryController@create');
            Route::post('/','CategoryController@store');
            Route::get('/{admin}','CategoryController@show');
            Route::get('/{admin}/edit','CategoryController@edit');
            Route::put('/{admin}','CategoryController@update');
            Route::delete('/{admin}','CategoryController@destroy');
            Route::get('/option/export','CategoryController@export');
            Route::get('/{id}/activation','CategoryController@activation');
        });
        Route::group([
            'prefix'=>'universities'
        ],function () {
            Route::get('/','UniversityController@index');
            Route::get('/create','UniversityController@create');
            Route::post('/','UniversityController@store');
            Route::get('/{university}','UniversityController@show');
            Route::get('/{university}/edit','UniversityController@edit');
            Route::put('/{university}','UniversityController@update');
            Route::delete('/{university}','UniversityController@destroy');
            Route::get('/option/export','UniversityController@export');
            Route::get('/{id}/activation','UniversityController@activation');
        });
        Route::group([
            'prefix'=>'brands'
        ],function () {
            Route::get('/','BrandController@index');
            Route::get('/create','BrandController@create');
            Route::post('/','BrandController@store');
            Route::get('/{brand}','BrandController@show');
            Route::get('/{brand}/edit','BrandController@edit');
            Route::put('/{brand}','BrandController@update');
            Route::delete('/{brand}','BrandController@destroy');
            Route::get('/option/export','BrandController@export');
        });
        Route::group([
            'prefix'=>'offers'
        ],function () {
            Route::get('/','OfferController@index');
            Route::get('/create','OfferController@create');
            Route::post('/','OfferController@store');
            Route::get('/{offer}','OfferController@show');
            Route::get('/{offer}/edit','OfferController@edit');
            Route::put('/{offer}','OfferController@update');
            Route::delete('/{offer}','OfferController@destroy');
            Route::get('/option/export','OfferController@export');
            Route::get('/{id}/activation','OfferController@activation');

        });
    });

    /*
    |--------------------------------------------------------------------------
    | Admin > App Management
    |--------------------------------------------------------------------------
    | Here is where App Management routes
    */

    Route::group([
        'prefix'=>'app_users',
        'namespace'=>'AppUser',
    ],function () {
        Route::group([
            'prefix'=>'users'
        ],function () {
            Route::get('/','UserController@index');
            Route::get('/create','UserController@create');
            Route::post('/','UserController@store');
            Route::get('/{user}','UserController@show');
            Route::get('/{user}/edit','UserController@edit');
            Route::put('/{user}','UserController@update');
            Route::delete('/{user}','UserController@destroy');
            Route::patch('/update/password',  'UserController@updatePassword');
            Route::get('/option/export','UserController@export');
            Route::get('/{id}/activation','UserController@activation');
        });

        Route::group([
            'prefix'=>'logs'
        ],function () {
            Route::get('/','LogController@index');
            Route::get('/option/export','LogController@export');
        });
    });

    /*
    |--------------------------------------------------------------------------
    | Admin > App Report
    |--------------------------------------------------------------------------
    | Here is where App Report routes
    */

    Route::group([
        'prefix'=>'app_reports',
        'namespace'=>'AppReport',
    ],function () {
        Route::group([
            'prefix'=>'emp_reports'
        ],function () {
            Route::get('/','EmployeeController@index');
            Route::get('/option/export','EmployeeController@export');
        });
        Route::group([
            'prefix'=>'user_reports'
        ],function () {
            Route::get('/','UserController@index');
            Route::get('/option/export','UserController@export');
        });
        Route::group([
            'prefix'=>'offer_reports'
        ],function () {
            Route::get('/','OfferController@index');
            Route::get('/option/export','OfferController@export');
        });
    });

});
