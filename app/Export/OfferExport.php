<?php

namespace App\Exports;

use App\Models\Offer;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;

/**
 * @property null type
 */
class OfferExport implements FromCollection
{
    /**
     * TargetsExport constructor.
     */
    public function __construct()
    {
        //
    }

    /**
    * @return Offer[]|Collection
    */
    public function collection()
    {
        return Offer::all();
    }
}
