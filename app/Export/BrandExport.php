<?php

namespace App\Exports;

use App\Models\Brand;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;

/**
 * @property null type
 */
class BrandExport implements FromCollection
{
    /**
     * TargetsExport constructor.
     */
    public function __construct()
    {
        //
    }

    /**
    * @return Brand[]|Collection
    */
    public function collection()
    {
        return Brand::all();
    }
}
