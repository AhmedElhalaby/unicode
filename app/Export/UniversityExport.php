<?php

namespace App\Exports;

use App\Models\University;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;

/**
 * @property null type
 */
class UniversityExport implements FromCollection
{
    /**
     * TargetsExport constructor.
     */
    public function __construct()
    {
        //
    }

    /**
    * @return University[]|Collection
    */
    public function collection()
    {
        return University::all();
    }
}
