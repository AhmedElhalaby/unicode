<?php

namespace App\Exports;

use App\Models\Log;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;

/**
 * @property null type
 */
class LogExport implements FromCollection
{
    /**
     * TargetsExport constructor.
     */
    public function __construct()
    {
        //
    }

    /**
    * @return Log[]|Collection
    */
    public function collection()
    {
        return Log::all();
    }
}
