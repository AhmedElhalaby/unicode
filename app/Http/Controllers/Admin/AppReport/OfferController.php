<?php

namespace App\Http\Controllers\Admin\AppReport;

use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\OfferReport\ExportRequest;
use App\Http\Requests\Admin\OfferReport\SearchRequest;
use App\Master;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class OfferController extends Controller
{
    protected $view_index = 'admin.offer_reports.index';
    protected $view_export = 'admin.offer_reports.export';
    protected $Params = [
        'Names'=>'Models/OfferReport.reports',
        'TheName'=>'Models/OfferReport.the_report',
        'Name'=>'Models/OfferReport.report',
        'redirect'=>'admin/app_reports/offer_reports',
    ];

    /**
     * Display a listing of the resource.
     *
     * @param SearchRequest $request
     * @return Factory|View
     */
    public function index(SearchRequest $request)
    {
        return $request->preset($this->view_index,$this->Params);
    }

    /**
     * Export resource in storage.
     *
     * @param ExportRequest $request
     * @return Master|Factory|View|BinaryFileResponse
     */
    public function export(ExportRequest $request)
    {
        return  $request->preset($this->view_export,$this->Params);
    }
}
