<?php

namespace App\Http\Controllers\Admin\AppReport;

use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\Report\ExportRequest;
use App\Http\Requests\Admin\Report\SearchRequest;
use App\Master;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class EmployeeController extends Controller
{
    protected $view_index = 'admin.emp_reports.index';
    protected $view_export = 'admin.emp_reports.export';
    protected $Params = [
        'Names'=>'Models/EmpReport.reports',
        'TheName'=>'Models/EmpReport.the_report',
        'Name'=>'Models/EmpReport.report',
        'redirect'=>'admin/app_reports/emp_reports',
    ];

    /**
     * Display a listing of the resource.
     *
     * @param SearchRequest $request
     * @return Factory|View
     */
    public function index(SearchRequest $request)
    {
        return $request->preset($this->view_index,$this->Params);
    }

    /**
     * Export resource in storage.
     *
     * @param ExportRequest $request
     * @return Master|Factory|View|BinaryFileResponse
     */
    public function export(ExportRequest $request)
    {
        return  $request->preset($this->view_export,$this->Params);
    }
}
