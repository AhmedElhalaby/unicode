<?php

namespace App\Http\Controllers\Admin\AppReport;

use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\UserReport\ExportRequest;
use App\Http\Requests\Admin\UserReport\SearchRequest;
use App\Master;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class UserController extends Controller
{
    protected $view_index = 'admin.user_reports.index';
    protected $view_export = 'admin.user_reports.export';
    protected $Params = [
        'Names'=>'Models/UserReport.reports',
        'TheName'=>'Models/UserReport.the_report',
        'Name'=>'Models/UserReport.report',
        'redirect'=>'admin/app_reports/user_reports',
    ];

    /**
     * Display a listing of the resource.
     *
     * @param SearchRequest $request
     * @return Factory|View
     */
    public function index(SearchRequest $request)
    {
        return $request->preset($this->view_index,$this->Params);
    }

    /**
     * Export resource in storage.
     *
     * @param ExportRequest $request
     * @return Master|Factory|View|BinaryFileResponse
     */
    public function export(ExportRequest $request)
    {
        return  $request->preset($this->view_export,$this->Params);
    }
}
