<?php

namespace App\Http\Controllers\Admin;

use App\Master;
use App\Models\User;
use App\Models\Notification;
use App\Notifications\VerifyAccount;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class HomeController extends Controller
{

    /**
     * @return Factory|View
     */
    public function index(){
        return view('admin.home');
    }
    public function general_notification(Request $request){
        $Title = $request->has('title')?$request->title:'';
        $Message = $request->has('msg')?$request->msg:'';
        foreach (User::all() as $user){
            if($user->device_token != null)
                Master::sendNotification($user->id,$user->device_token,$Title,$Message,$Title,$Message,null,Notification::TYPE['General']);
        }
        return redirect()->back()->with('status', __('admin.messages.notification_sent'));
    }
}
