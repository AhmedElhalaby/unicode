<?php

namespace App\Http\Controllers\Admin\AppUser;

use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\UserLog\ExportRequest;
use App\Http\Requests\Admin\UserLog\SearchRequest;
use App\Master;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class LogController extends Controller
{
    protected $view_index = 'admin.user_logs.index';
    protected $view_export = 'admin.user_logs.export';
    protected $Params = [
        'Names'=>'Models/Log.logs',
        'TheName'=>'Models/Log.the_log',
        'Name'=>'Models/Log.log',
        'redirect'=>'admin/app_users/logs',
    ];

    /**
     * Display a listing of the resource.
     *
     * @param SearchRequest $request
     * @return Factory|View
     */
    public function index(SearchRequest $request)
    {
        return $request->preset($this->view_index,$this->Params);
    }

    /**
     * Export resource in storage.
     *
     * @param ExportRequest $request
     * @return Master|Factory|View|BinaryFileResponse
     */
    public function export(ExportRequest $request)
    {
        return  $request->preset($this->view_export,$this->Params);
    }

}
