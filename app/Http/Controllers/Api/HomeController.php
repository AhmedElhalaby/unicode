<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\BrandResource;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Setting;
use App\Models\University;
use App\Traits\ResponseTrait;

class HomeController extends Controller
{
    use ResponseTrait;

    public function install(){
        $Universities = University::all();
        $Categories = Category::orderBy('order','asc')->get();
        $Brands = BrandResource::collection(Brand::all());
        $Settings = [];
        foreach (Setting::all() as $setting){
            $Settings[''.$setting->key] = [
                'name'=>(app()->getLocale() == 'ar')?$setting->name_ar:$setting->name,
                'text'=>(app()->getLocale() == 'ar')?$setting->text_ar:$setting->text
            ];
        }
        return $this->successJsonResponse([],[
            'Universities'=>$Universities,
            'Categories'=>$Categories,
            'Brands'=>$Brands,
            'Settings'=>$Settings
        ]);
    }
}
