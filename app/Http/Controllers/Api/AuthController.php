<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\Auth\ForgetPasswordRequest;
use App\Http\Requests\Api\Auth\LoginForm;
use App\Http\Requests\Api\Auth\PasswordRequest;
use App\Http\Requests\Api\Auth\RefreshForm;
use App\Http\Requests\Api\Auth\RegistrationForm;
use App\Http\Requests\Api\Auth\ResendVerifyForm;
use App\Http\Requests\Api\Auth\ResetPasswordRequest;
use App\Http\Requests\Api\Auth\UserRequest;
use App\Http\Requests\Api\Auth\VerifyForm;
use App\Models\Log;
use App\Traits\ResponseTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    use ResponseTrait;
    /**
     * Create user
     *
     * @param RegistrationForm $form
     * @return ResponseTrait
     */
    public function register(RegistrationForm $form)
    {
        return $form->persist();
    }

    /**
     * Login user and create token
     *
     * @param LoginForm $form
     * @return ResponseTrait
     */
    public function login(LoginForm $form)
    {
        return $form->persist();
    }

    /**
     * Show user profile
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function show(Request $request)
    {
        $user = $request->user();
        if (!$user->is_active)
            return $this->failJsonResponse([__('auth.blocked')]);
        $user->last_login_at = now();
        $user->save();
        $user['access_token']= $request->bearerToken();
        $user['token_type']= 'Bearer';
        return $this->successJsonResponse([],$user,'User');
    }

    /**
     * Logout user (Revoke the token)
     *
     * @param Request $request
     * @return ResponseTrait
     */
    public function logout(Request $request)
    {
        Log::create(['admin_id'=>$request->user()->id,'type'=>Log::$Type['User Logout']]);
        $request->user()->update(['device_token'=>null,'device_type'=>null]);
        $request->user()->token()->revoke();
        $request->user()->token()->delete();
        return $this->successJsonResponse([__('auth.logout')]);
    }

    /**
     * Update user profile
     *
     * @param UserRequest $request
     * @return  ResponseTrait
     */
    public function update(UserRequest $request)
    {
        return $request->persist();
    }

    /**
     * Refresh device token
     *
     * @param RefreshForm $request
     * @return  ResponseTrait
     */
    public function refresh(RefreshForm $request){
         return $request->persist();
    }

    /**
     * verify account
     *
     * @param ResendVerifyForm $request
     * @return  ResponseTrait
     */
    public function resend_verify(ResendVerifyForm $request){
        return $request->persist();
    }
    /**
     * verify account
     *
     * @param VerifyForm $request
     * @return  ResponseTrait
     */
    public function verify(VerifyForm $request){
         return $request->persist();
    }

    /**
     * @param PasswordRequest $request
     * @return JsonResponse
     */
    public function change_password(PasswordRequest $request){
        return $request->persist();
    }

    /**
     * @param ForgetPasswordRequest $request
     * @return JsonResponse
     */
    public function forget_password(ForgetPasswordRequest $request){
        return $request->persist();
    }

    /**
     * @param ResetPasswordRequest $request
     * @return JsonResponse
     */
    public function reset_password(ResetPasswordRequest $request){
        return $request->persist();
    }
}
