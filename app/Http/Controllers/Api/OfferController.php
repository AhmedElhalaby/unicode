<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Offer\SearchRequest;
use App\Http\Resources\ShowOfferResource;
use App\Models\Log;
use App\Models\Offer;
use App\Traits\ResponseTrait;
use Carbon\Carbon;

class OfferController extends Controller
{
    use ResponseTrait;

    public function index(SearchRequest $request){
        return $request->preset();
    }
    public function show($offer){
        $Offer = Offer::find($offer);
        if($Offer){
            if(auth('api')->check()){
                $log = Log::where('admin_id',auth('api')->user()->id)->where('type',Log::$Type['Show Offer'])->where('ref_id',$Offer->id)->orderBy('created_at','desc')->first();
                if($log){
                    $showDate = Carbon::parse($log->created_at)->addDays($Offer->reuse_day)->addHours($Offer->reuse_hour);
                    if($showDate > now()){
                        $code = 403;
                        return $this->failJsonResponse([__('You can`t see the code right now')],[],'Offer',null,$code);
                    }
                }
                $code = 200;
                Log::create(['admin_id'=>auth('api')->user()->id,'type'=>Log::$Type['Show Offer'],'ref_id'=>$Offer->id]);
                return $this->successJsonResponse([],new ShowOfferResource($Offer),'Offer',null,$code);
            }else{
                $code = 401;
                return $this->successJsonResponse([],new ShowOfferResource($Offer),'Offer',null,$code);

            }
        }
        else
            $code = 404;
        return $this->failJsonResponse([],[],'data',null,$code);
    }
}
