<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Favourite\SearchRequest;
use App\Http\Requests\Api\Favourite\ToggleRequest;
use App\Traits\ResponseTrait;

class FavouriteController extends Controller
{
    use ResponseTrait;

    public function index(SearchRequest $request){
        return $request->preset();
    }
    public function toggle(ToggleRequest $request){
        return $request->preset();
    }
}
