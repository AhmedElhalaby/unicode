<?php

namespace App\Http\Requests\Admin\Brand;

use App\Models\Brand;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'category_id' => 'required|exists:categories,id',
            'image' => 'mimes:jpeg,jpg,bmp,png',
        ];
    }
    public function preset($redirect,$id){
        $Object = Brand::find($id);
        if(!$Object)
            return redirect($redirect)->withErrors(__('admin.messages.wrong_data'));
        $Object->name = $this->name;
        if($this->hasFile('image')){
            $Object->setImage($this->file('image'));
        }
        $Object->category_id = $this->category_id;
        $Object->save();
        return redirect($redirect)->with('status', __('admin.messages.saved_successfully'));
    }
}
