<?php

namespace App\Http\Requests\Admin\University;

use App\Models\Brand;
use App\Models\University;
use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }
    public function preset($view,$params){
        $Objects = new University();
        if($this->has('q')){
            $Objects = $Objects->where('name','LIKE','%'.$this->q.'%')->orWhere('name_ar','LIKE','%'.$this->q.'%');
        }
        if($this->filled('name')){
            $Objects = $Objects->where('name','LIKE','%'.$this->name.'%');
        }
        if($this->filled('name_ar')){
            $Objects = $Objects->where('name_ar','LIKE','%'.$this->name_ar.'%');
        }

        $Objects = $Objects->paginate(($this->per_page)?$this->per_page:10);
        return view($view,compact('Objects'))->with($params);
    }
}
