<?php

namespace App\Http\Requests\Admin\UserReport;

use App\Models\Admin;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }
    public function preset($view,$params){
        $Objects = new User();
        if($this->filled('q')){
            $Objects = $Objects->where('first_name','LIKE','%'.$this->q.'%')->orwhere('second_name','LIKE','%'.$this->q.'%')->orwhere('email','LIKE','%'.$this->q.'%');
        }
        if($this->filled('first_name')){
            $Objects = $Objects->where('first_name','LIKE','%'.$this->first_name.'%');
        }
        if($this->filled('second_name')){
            $Objects = $Objects->where('second_name','LIKE','%'.$this->second_name.'%');
        }
        if($this->filled('mobile')){
            $Objects = $Objects->where('mobile','LIKE','%'.$this->mobile.'%');
        }
        if($this->filled('email')){
            $Objects = $Objects->where('email','LIKE','%'.$this->email.'%');
        }
        if($this->filled('university_id')){
            $Objects = $Objects->where('university_id',$this->university_id);
        }
        if($this->filled('is_active')){
            $Objects = $Objects->where('is_active',$this->is_active);
        }
        $Objects = $Objects->paginate(($this->per_page)?$this->per_page:10);
        return view($view,compact('Objects'))->with($params);
    }
}
