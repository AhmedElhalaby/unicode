<?php

namespace App\Http\Requests\Admin\Offer;

use App\Models\Log;
use App\Models\Offer;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'brand_id'=>'required|exists:brands,id',
            'category_id'=>'required|exists:categories,id',
            'description' => 'required|string|max:255',
            'name' => 'required|string|max:255',
            'expire_at' => 'required|date',
            'website' => 'required|string',
            'code' => 'required_without:barcode|max:255',
            'barcode' => 'required_without:code|max:255',
            'image' => 'mimes:jpeg,jpg,bmp,png',
            'reuse_day' => 'required|numeric',
            'reuse_hour' => 'required|numeric',

        ];
    }
    public function preset($redirect){
        $Object = new Offer();

        $Object->brand_id = $this->brand_id;
        $Object->category_id = $this->category_id;
        $Object->description = $this->description;
        $Object->name = $this->name;
        $Object->expire_at = $this->expire_at;
        $Object->website = $this->website;
        $Object->code = @$this->code;
        $Object->barcode = @$this->barcode;
        $Object->reuse_day = $this->reuse_day;
        $Object->reuse_hour = $this->reuse_hour;
        if ($this->hasFile('image')){
            $Object->setImage($this->file('image'));
        }
        $Object->is_active= auth('admin')->user()->permission == 3;
        $Object->created_by=auth('admin')->user()->id;
        $Object->updated_by=auth('admin')->user()->id;
        $Object->save();
        Log::create(['admin_id'=>auth('admin')->user()->id,'type'=>Log::$Type['Create Offer'],'ref_id'=>$Object->id]);

        return redirect($redirect)->with('status', __('admin.messages.saved_successfully'));
    }
}
