<?php

namespace App\Http\Requests\Admin\Offer;

use App\Models\Offer;
use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'brand_id'=>'exists:brands,id',
            'category_id'=>'exists:categories,id',
        ];
    }
    public function preset($view,$params){
        $Objects = new Offer();
        if($this->has('q')){
            $Objects = $Objects->where('description','LIKE','%'.$this->q.'%')->orWhere('website','LIKE','%'.$this->q.'%');
        }
        if($this->has('admin_id')){
            $Objects = $Objects->where('created_by',$this->admin_id)->orWhere('updated_by',$this->admin_id);
        }
        if($this->has('brand_id')){
            $Objects = $Objects->where('brand_id',$this->brand_id);
        }
        if($this->has('category_id')){
            $Objects = $Objects->where('category_id',$this->category_id);
        }
        if($this->has('description')){
            $Objects = $Objects->where('description','LIKE','%'.$this->description.'%');
        }
        if($this->has('code')){
            $Objects = $Objects->where('code','LIKE','%'.$this->code.'%');
        }
        if($this->has('is_active')){
            $Objects = $Objects->where('is_active','LIKE','%'.$this->is_active.'%');
        }
        if($this->has('website')){
            $Objects = $Objects->where('website','LIKE','%'.$this->website.'%');
        }
        $Objects = $Objects->orderBy('created_at','desc');
        $Objects = $Objects->paginate(($this->per_page)?$this->per_page:10);
        return view($view,compact('Objects'))->with($params);
    }
}
