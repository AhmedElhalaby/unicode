<?php

namespace App\Http\Requests\Admin\Offer;

use App\Models\Log;
use App\Models\Offer;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'brand_id'=>'required|exists:brands,id',
            'category_id'=>'required|exists:categories,id',
            'description' => 'required|string|max:255',
            'name' => 'required|string|max:255',
            'expire_at' => 'required|date',
            'website' => 'required|string',
            'code' => 'required_without:barcode|max:255',
            'barcode' => 'required_without:code|max:255',
            'image' => 'mimes:jpeg,jpg,bmp,png',
            'reuse_day' => 'required|numeric',
            'reuse_hour' => 'required|numeric',

        ];
    }
    public function preset($redirect,$id){
        $Object = Offer::where('id',$id)->first();
        if(!$Object)
            return redirect($redirect)->with('status', __('messages.object_not_found'));
        $Object->brand_id = $this->brand_id;
        $Object->category_id = $this->category_id;
        $Object->description = $this->description;
        $Object->name = $this->name;
        $Object->expire_at = $this->expire_at;
        $Object->website = $this->website;
        $Object->code = @$this->code;
        $Object->reuse_day = $this->reuse_day;
        $Object->reuse_hour = $this->reuse_hour;
        $Object->barcode = @$this->barcode;
        $Object->is_active=(auth('admin')->user()->permission == 3 )?true:false;
        $Object->updated_by = auth('admin')->user()->id;
        if($this->hasFile('image')){
            $Object->setImage($this->file('image'));
        }
        $Object->save();
        Log::create(['admin_id'=>auth('admin')->user()->id,'type'=>Log::$Type['Update Offer'],'ref_id'=>$Object->id]);

        return redirect($redirect)->with('status', __('admin.messages.saved_successfully'));
    }
}
