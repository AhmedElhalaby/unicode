<?php

namespace App\Http\Requests\Admin\User;

use App\Models\Admin;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|max:255',
            'second_name' => 'required|string|max:255',
            'email' => 'required|string|max:255|unique:users,email,'.$this->route('user'),
            'mobile' => 'required|string|max:255',
            'university_id' => 'required|exists:universities,id|max:255',
            'collage_student_number' => 'required|string|max:255',
            'collage_student_email' => 'required|email|max:255',
            'collage_graduation_years' => 'required|string|max:255',
        ];
    }
    public function preset($redirect,$id){
        $Object = Admin::find($id);
        if(!$Object)
            return redirect($redirect)->withErrors(__('admin.messages.wrong_data'));
        $Object->update($this->all());
        return redirect($redirect)->with('status', __('admin.messages.saved_successfully'));
    }
}
