<?php

namespace App\Http\Requests\Admin\User;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }
    public function preset($view,$params){
        $Objects = new User();
        if($this->filled('q')){
            $Objects = $Objects->where('name','LIKE','%'.$this->q.'%')->orwhere('email','LIKE','%'.$this->q.'%');
        }
        if($this->filled('name')){
            $Objects = $Objects->where('name','LIKE','%'.$this->name.'%');
        }
        if($this->filled('email')){
            $Objects = $Objects->where('email','LIKE','%'.$this->email.'%');
        }
        if($this->filled('is_active')){
            $Objects = $Objects->where('is_active','LIKE','%'.$this->is_active.'%');
        }
        $Objects = $Objects->orderBy('created_at','desc');

        $Objects = $Objects->paginate(($this->per_page)?$this->per_page:10);
        return view($view,compact('Objects'))->with($params);
    }
}
