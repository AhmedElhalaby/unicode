<?php

namespace App\Http\Requests\Admin\User;

use App\Models\Admin;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;

class StoreRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|max:255',
            'second_name' => 'required|string|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|max:255|confirmed|min:6',
            'mobile' => 'required|string|max:255',
            'university_id' => 'required|exists:universities,id|max:255',
            'collage_student_number' => 'required|string|max:255',
            'collage_student_email' => 'required|email|max:255',
            'collage_graduation_years' => 'required|string|max:255',
            ];
    }
    public function preset($redirect){
        $Object = User::create(array(
            'email' => $this->email,
            'first_name' => $this->first_name,
            'second_name' => $this->second_name,
            'mobile' => $this->mobile,
            'password' => Hash::make($this->password),
            'university_id' => $this->university_id,
            'collage_student_number' => $this->collage_student_number,
            'collage_student_email' => $this->collage_student_email,
            'collage_graduation_years' => $this->collage_graduation_years,
        ));
        return redirect($redirect)->with('status', __('admin.messages.saved_successfully'));
    }
}
