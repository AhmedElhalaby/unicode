<?php

namespace App\Http\Requests\Admin\Admin;

use App\Models\Admin;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;

class StoreRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|max:255',
            'second_name' => 'required|string|max:255',
            'third_name' => 'required|string|max:255',
            'password' => 'required|max:255|confirmed|min:6',
            'email' => 'required|email|max:255|unique:admins',
            'identity_num' => 'required|numeric|unique:admins',
//            'salary' => 'required|numeric',
            'permission' => 'required|in:1,2,3',
            'image' => 'mimes:jpeg,jpg,bmp,png',
            'identity' => 'mimes:jpeg,jpg,bmp,png',
            'passport' => 'mimes:jpeg,jpg,bmp,png',
        ];
    }
    public function preset($redirect){
        $Object = new Admin();
        $Object->first_name = $this->first_name;
        $Object->second_name = $this->second_name;
        $Object->third_name = $this->third_name;
        $Object->password = $this->password;
        $Object->email = $this->email;
        $Object->identity_num = $this->identity_num;
        $Object->salary = @$this->salary;
        $Object->permission = $this->permission;
        if ($this->hasFile('image')){
            $Object->setImage($this->file('image'));
        }
        if ($this->hasFile('identity')){
            $Object->setIdentity($this->file('identity'));
        }
        if ($this->hasFile('passport')){
            $Object->setPassport($this->file('passport'));
        }
        if ($this->hasFile('cv')){
            $Object->setCv($this->file('cv'));
        }
        $Object->save();
        return redirect($redirect)->with('status', __('admin.messages.saved_successfully'));
    }
}
