<?php

namespace App\Http\Requests\Admin\Admin;

use App\Models\Admin;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;

class UpdatePasswordRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required|max:255|confirmed|min:6',
            'id' => 'required|exists:admins,id',
        ];
    }
    public function preset($redirect){
        $Object = Admin::find($this->id);
        $Object->password = $this->password;
        $Object->save();
        return redirect($redirect)->with('status', __('admin.messages.saved_successfully'));
    }
}
