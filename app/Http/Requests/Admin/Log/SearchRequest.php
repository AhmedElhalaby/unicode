<?php

namespace App\Http\Requests\Admin\Log;

use App\Models\Log;
use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }
    public function preset($view,$params){
        $Objects = new Log();
        $Objects = $Objects->whereIn('type',[1,2,3,4]);
        if($this->filled('admin_id')){
            $Objects = $Objects->where('admin_id',$this->admin_id);
        }
        if($this->filled('type')){
            $Objects = $Objects->where('type', $this->type);
        }
        if($this->filled('created_at')){
            $Objects = $Objects->whereDate('created_at', $this->created_at);
        }
        $Objects = $Objects->paginate(($this->per_page)?$this->per_page:10);
        return view($view,compact('Objects'))->with($params);
    }
}
