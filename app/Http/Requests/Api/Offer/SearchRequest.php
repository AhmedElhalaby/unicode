<?php

namespace App\Http\Requests\Api\Offer;

use App\Http\Requests\Api\ApiRequest;
use App\Models\Offer;

class SearchRequest extends ApiRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'brand_id'=>'exists:brands,id',
            'category_id'=>'exists:categories,id',
        ];
    }
    public function preset(){
        $Objects = new Offer();
        if($this->has('q')){
            $Objects = $Objects->where('description','LIKE','%'.$this->q.'%')->orWhere('website','LIKE','%'.$this->q.'%');
        }
        if($this->has('brand_id')){
            $Objects = $Objects->where('brand_id',$this->brand_id);
        }
        if($this->has('category_id')){
            $Objects = $Objects->where('category_id',$this->category_id);
        }
        $Objects = $Objects->paginate(($this->per_page)?$this->per_page:10);
        if(auth('api')->check())
            $code = 200;
        else
            $code = 401;
        return $this->successJsonResponse([],$Objects->items(),'Offers',$Objects,$code);
    }
}
