<?php

namespace App\Http\Requests\Api\Favourite;

use App\Http\Requests\Api\ApiRequest;
use App\Models\Favourite;

class ToggleRequest extends ApiRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'offer_id' => 'required|exists:offers,id',
        ];
    }
    public function preset(){
        $Favourite = Favourite::where('offer_id',$this->offer_id)->where('user_id',$this->user()->id)->first();
        if($Favourite){
            $Favourite->delete();
            return $this->successJsonResponse([__('messages.unfav_successful')]);
        }
        else{
            $Object = Favourite::create(['offer_id'=>$this->offer_id,'user_id'=>$this->user()->id]);
            return $this->successJsonResponse([__('messages.fav_successful')],$Object,'Favourite');
        }
    }
}
