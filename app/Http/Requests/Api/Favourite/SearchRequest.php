<?php

namespace App\Http\Requests\Api\Favourite;

use App\Http\Requests\Api\ApiRequest;
use App\Models\Favourite;
use App\Models\Offer;

class SearchRequest extends ApiRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            ];
    }
    public function preset(){
        $Favourites = Favourite::where('user_id',$this->user()->id)->pluck('offer_id');
        $Objects = new Offer();
        $Objects = $Objects->whereIn('id',$Favourites);
        $Objects = $Objects->paginate(($this->per_page)?$this->per_page:10);
        return $this->successJsonResponse([],$Objects->items(),'Offers',$Objects);
    }
}
