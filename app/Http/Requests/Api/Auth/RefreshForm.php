<?php

namespace App\Http\Requests\Api\Auth;

use App\Http\Requests\Api\ApiRequest;
use App\Master;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;

class RefreshForm extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'device_type' => 'required|string',
            'device_token' => 'required|string'
        ];
    }
    public function attributes()
    {
        return Master::NiceNames('User');
    }
    public function persist()
    {
        $logged = auth('api')->user();
        $logged->device_token = $this->device_token;
        $logged->device_type = $this->device_type;
        $logged->save();
        $logged['access_token']= $this->bearerToken();
        $logged['token_type']= 'Bearer';
        return $this->successJsonResponse( [__('auth.refreshed')],$logged,'User');
    }
}
