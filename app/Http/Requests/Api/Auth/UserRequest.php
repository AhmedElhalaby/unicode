<?php

namespace App\Http\Requests\Api\Auth;

use App\Http\Requests\Api\ApiRequest;
use App\Master;
use App\Traits\ResponseTrait;
use App\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;

class UserRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'string|max:255,',
            'second_name' => 'string|max:255,',
            'email' => 'email|min:6|unique:users,id,'. auth()->user()->id,
            'mobile' => 'numeric|min:6',
            'university_id' => 'exists:universities,id',
            'collage_student_number' => 'string|max:255,',
            'collage_student_email' => 'email',
            'collage_graduation_years' => 'string|max:255,',
            'image' => 'mimes:jpeg,jpg,bmp,png,',
            'device_token' => 'string|required_with:device',
            'device_type' => 'string|required_with:device_token',

        ];
    }
    public function attributes()
    {
        return Master::NiceNames('User');
    }
    public function persist()
    {
        $logged = auth()->user();
        if ($this->filled('first_name')) {
            $logged->first_name = $this->first_name;
        }
        if ($this->filled('second_name')) {
            $logged->second_name = $this->second_name;
        }
        if ($this->filled('email')) {
            $logged->email = $this->email;
        }
        if ($this->filled('mobile')) {
            $logged->mobile = $this->mobile;
        }
        if ($this->filled('university_id')) {
            $logged->university_id = $this->university_id;
        }
        if ($this->filled('collage_student_number')) {
            $logged->collage_student_number = $this->collage_student_number;
        }
        if ($this->filled('collage_student_email')) {
            $logged->collage_student_email = $this->collage_student_email;
        }
        if ($this->filled('collage_graduation_years')) {
            $logged->collage_graduation_years = $this->collage_graduation_years;
        }
        if ($this->filled('device_token')) {
            $logged->device_token = $this->device_token;
        }
        if ($this->filled('device_type')) {
            $logged->device_type = $this->device_type;
        }
        if ($this->hasFile('image')) {
            $logged->setImage($this->file('image'));
        }
        $logged->save();
        $logged['access_token']= $this->bearerToken();
        $logged['token_type']= 'Bearer';
        return $this->successJsonResponse( [__('auth.update_successful')],$logged,'User');

    }
}
