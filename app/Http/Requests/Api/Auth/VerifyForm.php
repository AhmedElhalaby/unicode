<?php

namespace App\Http\Requests\Api\Auth;

use App\Http\Requests\Api\ApiRequest;
use App\Master;
use App\Models\User;
use App\Models\VerifyAccounts;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;

class VerifyForm extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'=>'required|exists:users,email|email',
            'code' => 'required|string',
        ];
    }
    public function attributes()
    {
        return Master::NiceNames('User');
    }
    public function persist()
    {
        $logged = User::where('email',$this->email)->first();
        $verify = VerifyAccounts::where('email',$logged->email)->first();
        if($this->code != $verify->code)
            return $this->failJsonResponse([__('auth.failed')]);
        if($logged->email_verified_at != null)
            return $this->failJsonResponse([__('auth.verified_before')]);
        $logged->email_verified_at = now();
        $logged->save();
        DB::table('oauth_access_tokens')->where('user_id', $logged->id)->delete();
        $tokenResult = $logged->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->save();
        $logged['access_token']= $tokenResult->accessToken;
        $logged['token_type']= 'Bearer';
        return $this->successJsonResponse( [__('auth.verified')],$logged,'User');
    }
}
