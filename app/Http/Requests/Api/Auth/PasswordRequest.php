<?php

namespace App\Http\Requests\Api\Auth;

use App\Http\Requests\Api\ApiRequest;
use App\Master;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class PasswordRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_password' => 'required|string|min:6',
            'password' => 'required|string|min:6|confirmed',
        ];
    }
    public function attributes()
    {
        return Master::NiceNames('User');
    }
    public function persist()
    {
        $logged = auth()->user();
        if(Hash::check($this->old_password,$logged->password)){
            $logged->password = $this->password;
            $logged->save();
            $logged['access_token']= $this->bearerToken();
            $logged['token_type']= 'Bearer';
            return $this->successJsonResponse([__('messages.updated_successful')],$logged ,'User');
        }
        return $this->failJsonResponse([__('auth.password_not_correct')]);
    }
}
