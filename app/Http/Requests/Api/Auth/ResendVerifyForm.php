<?php

namespace App\Http\Requests\Api\Auth;

use App\Http\Requests\Api\ApiRequest;
use App\Mail\VerificationMail;
use App\Master;
use App\Models\User;
use App\Models\VerifyAccounts;
use App\Notifications\VerifyAccount;
use App\Traits\ResponseTrait;
use Illuminate\Support\Str;

class ResendVerifyForm extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'=>'required|exists:users,email|email'
        ];
    }
    public function attributes()
    {
        return Master::NiceNames('User');
    }
    public function persist()
    {
        $logged = User::where('email',$this->email)->first();
        $code = rand( 10000 , 99999 );
        $token = Str::random(40).time();
        $VerifyAccounts = VerifyAccounts::updateOrCreate(
            ['email' => $logged->email],
            [
                'email' => $logged->email,
                'code' => $code,
                'token' => $token,
            ]
        );
        \Mail::to($logged)->send(new VerificationMail($token));
        return $this->successJsonResponse( [__('auth.verification_code_sent')]);
    }
}
