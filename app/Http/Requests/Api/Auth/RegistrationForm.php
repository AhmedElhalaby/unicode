<?php

namespace App\Http\Requests\Api\Auth;

use App\Http\Requests\Api\ApiRequest;
use App\Mail\VerificationMail;
use App\Master;
use App\Models\Log;
use App\Models\User;
use App\Models\VerifyAccounts;
use App\Notifications\VerifyAccount;
use App\Traits\ResponseTrait;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class RegistrationForm extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return Validator
     */
    protected function getValidatorInstance()
    {
        $data = $this->all();
        if($this->has('dob') && ($this->dob == '' || $this->dob == null)){
            unset($data['dob']);
        }
        if($this->has('gender') && ($this->gender == '' || $this->gender == null)){
            unset($data['gender']);
        }
        if($this->has('collage_student_email') && ($this->collage_student_email == "" || $this->collage_student_email == null)){
            unset($data['collage_student_email']);
        }
        $this->getInputSource()->replace($data);
        /*modify data before send to validator*/
        return parent::getValidatorInstance();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|max:255',
            'second_name' => 'required|string|max:255',
            'email' => 'required|email|min:6|unique:users',
            'mobile' => 'required|numeric|min:6',
            'password' => 'required|string|min:6',
            'university_id' => 'required|exists:universities,id',
            'collage_student_number' => 'sometimes|string',
            'collage_student_email' => 'sometimes|email',
            'collage_graduation_years' => 'sometimes|string',
            'device_token' => 'string|required_with:device',
            'device_type' => 'string|required_with:device_token',
        ];
    }
    public function attributes()
    {
        return Master::NiceNames('User');
    }
    public function persist()
    {
        $user = new User();
        $user->first_name = $this->first_name;
        $user->second_name = $this->second_name;
        $user->email = $this->email;
        $user->mobile = $this->mobile;
        $user->password = $this->password;
        $user->university_id = @$this->university_id;
        $user->collage_student_email = @$this->collage_student_email;
        $user->collage_student_number = @$this->collage_student_number;
        $user->collage_graduation_years = @$this->collage_graduation_years;
        $user->last_login_at = now();
        if ($this->input('device_token')) {
            $user->device_token = $this->device_token;
            $user->device_type = $this->device_type;
        }
        $user->save();
        Auth::attempt(request(['email', 'password']));
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->save();
        $user->refresh();
        $user['access_token']= $tokenResult->accessToken;
        $user['token_type']= 'Bearer';
        $code = rand( 10000 , 99999 );
        $token = Str::random(40).time();
        $VerifyAccounts = VerifyAccounts::updateOrCreate(
            ['email' => $this->email],
            [
                'email' => $this->email,
                'code' => $code,
                'token' => $token,
            ]
        );
        try {
            \Mail::to($user)->send(new VerificationMail($token));
        }catch (\Exception $e){

        }
        Log::create(['admin_id'=>$user->id,'type'=>Log::$Type['User Login']]);
        return $this->successJsonResponse( [__('messages.saved_successfully')],$user,'User');
    }

}
