<?php

namespace App\Http\Resources;

use App\Models\Favourite;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OfferResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $Object['id'] = $this->id;
        $Object['name'] = $this->name;
        $Object['brand_id'] = $this->brand_id;
        $Object['category_id'] = $this->category_id;
        $Object['description'] = $this->description;
        $Object['expire_at'] = $this->expire_at;
        $Object['website'] = $this->website;
        $Object['image'] = $this->getImage();
        $Object['is_active'] = $this->is_active;
        $Object['created_by'] = $this->created_by;
        $Object['updated_by'] = $this->updated_by;
        $Object['reuse_hour'] = $this->reuse_hour;
        $Object['reuse_day'] = $this->reuse_day;
        $Object['Brand'] = ($this->brand)?new BrandResource($this->brand):null;
        if (auth('api')->check()){
            $Object['is_favourite'] = Favourite::where('user_id',auth('api')->user()->id)->where('offer_id',$this->id)->first() ? true:false;
        }else{
            $Object['is_favourite'] =false;
        }
        $Object['created_at'] =$this->created_at;
        $Object['updated_at'] =$this->updated_at;
        return $Object;
    }
}
