<?php

namespace App\Models;

use App\Master;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * @property mixed image
 */
class Offer extends Model
{
    protected $table = 'offers';
    protected $fillable = ['name','brand_id','category_id','description','expire_at','website','code','barcode','image','is_active','created_by','updated_by','reuse_hour','reuse_day'];
    protected $appends = ['Brand','is_favourite','is_favourite_str'];
    protected $hidden = ['code','barcode'];
    public function brand(){
        return $this->belongsTo(Brand::class);
    }
    public function category(){
        return $this->belongsTo(Category::class);
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image): void
    {
        $this->image = Master::UploadModel($image,'offers/image');
    }


    public function getBrandAttribute()
    {
        return $this->brand()->first();
    }
    public function getBarcodeAttribute($value)
    {
        return (auth('api')->user()||auth('admin')->user())?$value:__('auth.unauthenticated');
    }
    public function getCodeAttribute($value)
    {
        return (auth('api')->user()||auth('admin')->user())?$value:__('auth.unauthenticated');
    }
    public function getImageAttribute($value)
    {
        return ($value)?asset($value):null;
    }
    public function getIsFavouriteAttribute($value)
    {
        if(auth('api')->check()){
            $Favourite = Favourite::where('user_id',auth('api')->user()->id)->where('offer_id',$this->id)->first();
            if($Favourite)
                return true;
        }
        return false;
    }
    public function getIsFavouriteStrAttribute($value)
    {
        if(auth('api')->check()){
            $Favourite = Favourite::where('user_id',auth('api')->user()->id)->where('offer_id',$this->id)->first();
            if($Favourite)
                return "true";
        }
        return "false";
    }
}
