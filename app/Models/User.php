<?php

namespace App\Models;

use App\Master;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable,HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'second_name',
        'email',
        'password',
        'mobile',
        'email_verified_at',
        'university_id',
        'collage_student_number',
        'collage_student_email',
        'collage_graduation_years',
        'device_type',
        'device_token',
        'image',
        'last_login_at',
        'is_active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
//        'verification_code'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function university(){
        return $this->belongsTo(University::class);
    }
    public function offers_count(){
        return Log::where('admin_id',$this->id)->where('type',Log::$Type['Show Offer'])->count();
    }
    public function last_login(){
        return (Log::where('admin_id',$this->id)->where('type',Log::$Type['User Login'])->orderBy('created_at','desc')->first())?Log::where('admin_id',$this->id)->where('type',Log::$Type['User Login'])->orderBy('created_at','desc')->first()->created_at:'';
    }
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }
    /**
     * @param mixed $image
     */
    public function setImage($image): void
    {
        $this->image = Master::UploadModel($image,'users/image');
    }

    public function getImageAttribute($value)
    {
        return ($value)?asset($value):null;
    }
}
