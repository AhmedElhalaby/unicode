<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Log extends Model
{
    protected $table = 'logs';
    protected $fillable = ['admin_id','type','ref_id'];

    public function admin(){
        return $this->belongsTo(Admin::class);
    }
    public function user(){
        return $this->belongsTo(User::class,'admin_id');
    }
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('created_at', 'desc');
        });
    }
    public static $Type =[
        'Login'=>1,
        'Logout'=>2,
        'Create Offer'=>3,
        'Update Offer'=>4,
        'User Login'=>5,
        'User Logout'=>6,
        'Show Offer'=>7
    ];
    public static $TypeString =[
        1=>'Login',
        2=>'Logout',
        3=>'Create_Offer',
        4=>'Update_Offer',
        5=>'Login',
        6=>'Logout',
        7=>'Show_Offer',
    ];

}
