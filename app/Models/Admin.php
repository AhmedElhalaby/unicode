<?php

namespace App\Models;

use App\Master;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

/**
 * @property mixed image
 * @property mixed cv
 * @property mixed identity
 * @property mixed passport
 */
class Admin extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'first_name','second_name','third_name','identity_num','email','password','remember_token','is_active','permission','image','cv','identity','passport','salary'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public static $Permission=[
        'Employee'=>1,
        'Supervisor'=>2,
        'Admin'=>3
    ];
    public static $PermissionString=[
        1=>'Employee',
        2=>'Supervisor',
        3=>'Admin'
    ];

    /**
     * @return mixed
     */
    public function getImage()
    {
        return ($this->image)?asset($this->image):null;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image): void
    {
        $this->image = Master::UploadModel($image,'users/image');
    }

    /**
     * @return mixed
     */
    public function getCv()
    {
        return ($this->cv)?asset($this->cv):null;
    }

    /**
     * @param mixed $cv
     */
    public function setCv($cv): void
    {
        $this->cv = Master::UploadModel($cv,'users/cv');
    }

    /**
     * @return mixed
     */
    public function getIdentity()
    {
        return ($this->identity)?asset($this->identity):null;
    }

    /**
     * @param mixed $identity
     */
    public function setIdentity($identity): void
    {
        $this->identity = Master::UploadModel($identity,'users/identity');
    }

    /**
     * @return mixed
     */
    public function getPassport()
    {
        return ($this->passport)?asset($this->passport):null;
    }

    /**
     * @param mixed $passport
     */
    public function setPassport($passport): void
    {
        $this->passport = Master::UploadModel($passport,'users/passport');
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }
    public function offers_count(){
        return Offer::where('created_by',$this->id)->count();
    }
    public function last_login(){
        return (Log::where('admin_id',$this->id)->where('type',Log::$Type['Login'])->orderBy('created_at','desc')->first())?Log::where('admin_id',$this->id)->where('type',Log::$Type['Login'])->orderBy('created_at','desc')->first()->created_at:'';
    }
}
