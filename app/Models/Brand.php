<?php

namespace App\Models;

use App\Master;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * @property mixed image
 */
class Brand extends Model
{
    protected $table = 'brands';
    protected $fillable = ['name','image','category_id'];

    public function category(){
        return $this->belongsTo(Category::class);
    }
    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image): void
    {
        $this->image = Master::UploadModel($image,'brands/image');
    }

    public function getImageAttribute($value)
    {
        return ($value)?asset($value):null;
    }
}
