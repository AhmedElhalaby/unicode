<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{

    protected $table = 'notifications';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'user_id','title','message','title_ar','message_ar','ref_id','type','read_at'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    const TYPE = [
        'General'=>0,
    ];

}
