<?php


namespace App;


use App\Models\Attachment;
use App\Models\Notification;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Storage;
use Mpdf\Mpdf;
use Mpdf\MpdfException;

class Master
{
    public static function NiceNames($Model){
        switch ($Model){
            case 'User':
                return [
                    'name'=>__('names.users.name'),
                    'email'=>__('names.users.email'),
                    'mobile'=>__('names.users.mobile'),
                    'password'=>__('names.users.password'),
                    'old_password'=>__('names.users.old_password'),
                    'password_confirmation'=>__('names.users.password_confirmation'),
                    'address'=>__('names.users.address'),
                    'city_id'=>__('names.users.city_id'),
                    'gender'=>__('names.users.gender'),
                    'dob'=>__('names.users.dob'),
                    'type'=>__('names.users.type'),
                    'verified_at'=>__('names.users.verified_at'),
                    'device_token'=>__('names.users.device_token'),
                    'device'=>__('names.users.device'),
                    'verification_code'=>__('names.users.verification_code'),
                    'is_active'=>__('names.users.is_active'),
                ];
            case 'Order':
                return [
                    'title'=>__('names.advertisements.title'),
                    'description'=>__('names.advertisements.description'),
                    'category_id'=>__('names.advertisements.category_id'),
                    'city_id'=>__('names.advertisements.city_id'),
                    'image'=>__('names.advertisements.image'),
                    'message'=>__('names.advertisements.message'),
                    'stars'=>__('names.advertisements.stars'),
                    'sub_category_id'=>__('names.advertisements.sub_category_id'),
                ];
            case 'Comment':
                return [
                    'comment'=>__('names.comments.comment'),
                ];
            case 'Favourite':
                return [
                    'place_id'=>__('names.favourites.place_id'),
                ];
            case 'Offer':
                return [
                    'Offer'=>__('names.favourites.place_id'),
                ];
            default :
                [];
        }

    }

    public static function UploadModel( $value, $destination_path){
        $destination_path = "public/".$destination_path.'/';
        if($value){
            $file = $value;
            $new_file_name = md5($file->getClientOriginalName().time()).'.'.$file->getClientOriginalExtension();
            $file_path = $file->move($destination_path, $new_file_name);
            return $destination_path.$new_file_name;
        }
        return null;
    }
    public static function Upload($attribute_name,$destination_path){
        $destination_path = "storage/".$destination_path.'/';
        $request = Request::instance();
        if ($request->hasFile($attribute_name)) {
            $file = $request->file($attribute_name);
            if ($file->isValid()) {
                $file_name = md5($file->getClientOriginalName().time()).'.'.$file->getClientOriginalExtension();
                $isImageURL = Storage::disk('s3')->put($destination_path.$file_name,$file, "public");
                $attribute_value =  $destination_path.$file_name;
            }
        }
        return $attribute_value??null;
    }
    public static function MultiUpload($attribute_name, $destination_path,$ref_id,$type=1){
        $request = \Request::instance();
        $destination_path = "public/".$destination_path.'/';

        // if a new file is uploaded, store it on disk and its filename in the database
        if ($request->hasFile($attribute_name)) {
            $file = $request->file($attribute_name);

            if(is_array($file)){
                foreach ($file as $item){
                    // 1. Generate a new file name
                    $new_file_name = md5($item->getClientOriginalName().time()).'.'.$item->getClientOriginalExtension();
                    // 2. Move the new file to the correct path
                    $file_path = $item->move($destination_path, $new_file_name);
                    // 3. Save the complete path to the database
                    Attachment::create(array('ref_id'=>$ref_id,'type'=>$type,'attachment'=>$destination_path.$new_file_name));
                }
                return true;
            }else{
                // 1. Generate a new file name
                $file = $request->file($attribute_name);
                $new_file_name = md5($file->getClientOriginalName().time()).'.'.$file->getClientOriginalExtension();
                // 2. Move the new file to the correct path
                $file_path = $file->move($destination_path, $new_file_name);
                // 3. Save the complete path to the database
                return $destination_path.$new_file_name;
            }
        }
        return false;
    }
    public static function SocialLogin($Provider,$Token){
        switch ($Provider){
            case 'google':{
                $User = [];
                $smsUrl = 'https://oauth2.googleapis.com/tokeninfo?id_token='.$Token;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$smsUrl);
                curl_setopt($ch, CURLOPT_POST, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                $result = curl_exec($ch);
                curl_close($ch);
                $result = json_decode($result,true);
                if (isset($result['error']))
                    return null;
                $User['provider_id'] = @$result['sub'];
                $User['name'] = @$result['name'];
                $User['email'] = @$result['email'];
                return $User;
            }
            case 'facebook':{
                $Fields = 'email%2Cname';
                $smsUrl = 'https://graph.facebook.com/v4.0/me?access_token='.$Token.'&fields='.$Fields.'';
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$smsUrl);
                curl_setopt($ch, CURLOPT_POST, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                $result = curl_exec($ch);
                curl_close($ch);
                $result = json_decode($result,true);
                if (isset($result['error']))
                    return null;
                $User['provider_id'] = @$result['id'];
                $User['name'] = @$result['name'];
                $User['email'] = @$result['email'];
                return $User;
            }
            default:{
                return null;
            }
        }
    }

    public static function ExportPDF($Objects, $view, $names,  $save = false, $path = '')
    {
        $html = view($view, compact('Objects', 'names'))->render();
        try {
            $mpdf = new Mpdf([
                'default_font' => 'frutiger',
                'tempDir' => __DIR__ . '/tmp',
                // 'orientation' => 'A4'
            ]);
            $mpdf->SetProtection(array('print'));
            $mpdf->SetTitle($names);
            $mpdf->autoScriptToLang = true;
            $mpdf->baseScript = 1;
            $mpdf->autoVietnamese = true;
            $mpdf->autoArabic = true;
            $mpdf->autoLangToFont = true;
            $mpdf->showImageErrors = true;
            $mpdf->SetDirectionality('rtl');
            $mpdf->SetDisplayMode('fullpage');
            $mpdf->WriteHTML($html);
            if ($save) {
                $mpdf->Output($path, 'F');
            } else {
                $mpdf->Output($names.'-'.now(). '.pdf', 'D');
            }
        } catch (MpdfException $e) {
            return redirect()->back()->with('error','Error : '.$e->getMessage());
        }
    }

    public static function SendNotification($user_id,$token, $title,$msg,$title_ar,$msg_ar,$ref_id = null,$type= 0,$store = true)
    {
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $registrationIds = $token;


        $message = array
        (
            'body'  => $msg,
            'title' => $title,
            'sound' => true,
        );
        $extraNotificationData = ["ref_id" =>$ref_id,"type"=>$type];
        $fields = array
        (
            'to'        => $registrationIds,
            'notification'  => $message,
            'data' => $extraNotificationData
        );
        $headers = array
        (
            'Authorization: key='.config('app.notification_key') ,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
        if($store){
            $notify = new Notification();
            $notify->type = $type;
            $notify->user_id = $user_id;
            $notify->title = $title;
            $notify->message = $msg;
            $notify->title_ar = $title_ar;
            $notify->message_ar = $msg_ar;
            $notify->ref_id = @$ref_id;
            $notify->save();
        }
        return true;
    }

    public static function CheckActiveUsers(){
        $Users = User::where('is_active',true)->where('last_login_at','<',Carbon::now()->subMonth());
        foreach ($Users as $user){
            $user->is_active = false;
            $user->save();
        }
    }
}
