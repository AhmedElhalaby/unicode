<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'categories' => 'التصنيفات',
    'category' => 'تصنيف',
    'the_category' => 'التصنيف',
    'name' => 'الاسم',
    'name_ar' => 'الاسم بالعربي',
    'order' => 'الترتيب',
    'status' => 'الحالة',
    'active' => 'مفعل',
    'do_active' => 'تفعيل',
    'in_active' => 'معطل',
    'do_in_active' => 'تعطيل',

];
