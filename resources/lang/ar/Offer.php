<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'offers' => 'العروض',
    'offer' => 'عرض',
    'the_offer' => 'العرض',
    'brand_id' => 'البراند',
    'category_id' => 'التصنيف',
    'name' => 'الاسم',
    'description' => 'الوصف',
    'expire_at' => 'انتهاء العرض',
    'website' => 'الموقع',
    'code' => 'الكود',
    'barcode' => 'الباركود',
    'image' => 'صورة العرض',
    'reuse_hour' => 'عدد ساعات اعادة الاستخدام',
    'reuse_day' => 'عدد ايام اعادة الاستخدام',
    'show_count' => 'مرات الظهور',

];
