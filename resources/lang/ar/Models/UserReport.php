<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'reports' => 'تقارير المستخدمين',
    'report' => 'تقرير المستخدم',
    'the_report' => 'التقرير',
    'user_id' => 'المستخدم',
    'num_of_codes' => 'عدد العروض المشاهدة',
    'last_login' => 'تاريخ أخر دخول',

];
