<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'admins' => 'الموظفين',
    'admin' => 'موظف',
    'the_admin' => 'الموظف',
    'permission' => 'الصلاحيات',
    'name' => 'الاسم',
    'first_name' => 'الاسم الأول',
    'second_name' => 'الاسم الثاني',
    'third_name' => 'الاسم الثالث',
    'identity_num' => 'رقم  الهوية',
    'email' => 'البريد الالكتروني',
    'status' => 'الحالة',
    'password' => 'كلمة المرور',
    'password_confirmation' => 'تأكيد كلمة المرور',
    'image' => 'الصورة الشخصية',
    'cv' => 'السيرة الذاتية',
    'identity' => 'الهوية الشخصية',
    'passport' => 'جواز السفر',
    'salary' => 'الراتب',
    'active' => 'مفعل',
    'do_active' => 'تفعيل',
    'in_active' => 'معطل',
    'do_in_active' => 'تعطيل',
    'Permission'=>[
        'Employee'=>'موظف',
        'Supervisor'=>'مشرف',
        'Admin'=>'مدير',
    ]
];
