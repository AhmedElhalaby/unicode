<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'users' => 'المستخدمين',
    'user' => 'مستخدم',
    'the_user' => 'المستخدم',
    'name' => 'Name',
    'first_name' => 'الاسم الأول',
    'second_name' => 'الاسم الثاني',
    'email' => 'البريد الالكتروني',
    'password' => 'كلمة المرور',
    'password_confirmation' => 'تأكيد كلمة المرور',
    'mobile' => 'رقم الجوال',
    'university_id' => 'الجامعة',
    'collage_student_number' => 'الرقم الجامعي',
    'collage_student_email' => 'البريد الجامعي',
    'collage_graduation_years' => 'سنة التخرج',
    'status' => 'الحالة',
    'active' => 'مفعل',
    'do_active' => 'تفعيل',
    'in_active' => 'معطل',
    'do_in_active' => 'تعطيل',
];
