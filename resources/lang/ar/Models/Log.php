<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'logs' => 'التحركات',
    'log' => 'حركة',
    'the_log' => 'الحركة',
    'admin_id' => 'الموظف',
    'user_id' => 'المستخدم',
    'type' => 'نوع العملية',
    'ref_id' => 'الارتباط',
    'created_at' => 'تاريخ العملية',
    'Type' =>[
        'Login'=>'تسجيل دخول',
        'Logout'=>'تسجيل خروج',
        'Create_Offer'=>'اضافة عرض',
        'Update_Offer'=>'تعديل عرض',
        'Show_Offer'=>'مشاهدة عرض',
    ],

];
