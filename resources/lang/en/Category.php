<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'categories' => 'Categories',
    'category' => 'Category',
    'the_category' => 'The Category',
    'name' => 'Name',
    'name_ar' => 'Name Ar',
    'order' => 'Order',
    'status' => 'Status',
    'active' => 'Active',
    'do_active' => 'Activate',
    'in_active' => 'In-Active',
    'do_in_active' => 'De-Activate',

];
