<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'logs' => 'Logs',
    'log' => 'Log',
    'the_log' => 'The Log',
    'admin_id' => 'Employee',
    'user_id' => 'User',
    'type' => 'Type',
    'ref_id' => 'Reference',
    'created_at' => 'Operation Date',
    'Type' =>[
        'Login'=>'Login',
        'Logout'=>'Logout',
        'Create_Offer'=>'Create Offer',
        'Update_Offer'=>'Update Offer',
        'Show_Offer'=>'View Offer',
    ],

];
