<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'reports' => 'Employee Reports',
    'report' => 'Employee Report',
    'the_report' => 'The Report',
    'admin_id' => 'Employee',
    'num_of_codes' => 'Offers Created',
    'last_login' => 'Last Login',

];
