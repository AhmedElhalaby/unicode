<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'users' => 'Users',
    'user' => 'User',
    'the_user' => 'The User',
    'name' => 'Name',
    'first_name' => 'First Name',
    'second_name' => 'Second Name',
    'email' => 'E-Mail',
    'password' => 'Password',
    'password_confirmation' => 'Password Confirmation',
    'mobile' => 'Mobile',
    'university_id' => 'University',
    'collage_student_number' => 'Student Number',
    'collage_student_email' => 'Student E-Mail',
    'collage_graduation_years' => 'Graduation Year',
    'status' => 'Status',
    'active' => 'Active',
    'do_active' => 'Activate',
    'in_active' => 'In-Active',
    'do_in_active' => 'In-Activate',
];
