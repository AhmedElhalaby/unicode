<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'admins' => 'Employees',
    'admin' => 'Employee',
    'the_admin' => 'The Employee',
    'permission' => 'Permission',
    'name' => 'Name',
    'first_name' => 'First Name',
    'second_name' => 'Second Name',
    'third_name' => 'Third Name',
    'identity_num' => 'Identity',
    'email' => 'E-Mail',
    'status' => 'Status',
    'image' => 'Image',
    'cv' => 'Cv',
    'identity' => 'Identity Image',
    'passport' => 'Passport',
    'salary' => 'Salary',
    'password' => 'Password',
    'password_confirmation' => 'Password Confirmation',
    'active' => 'Active',
    'do_active' => 'Activate',
    'in_active' => 'In-Active',
    'do_in_active' => 'De-Activate',
    'Permission'=>[
        'Employee'=>'Employee',
        'Supervisor'=>'Supervisor',
        'Admin'=>'Admin',
    ]
];
