<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'reports' => 'User Reports',
    'report' => 'User Report',
    'the_report' => 'The Report',
    'user_id' => 'User',
    'num_of_codes' => 'Offers Showed',
    'last_login' => 'Last Login',

];
