<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'object_not_found' => 'Object Not Found',
    'deleted_successful' => 'Deleted Successfully !',
    'created_successful' => 'Created Successfully !',
    'updated_successful' => 'Updated Successfully !',
    'rated_successful' => 'Rated Successfully !',
    'unfav_successful' => 'Removed From Favourite Successfully !',
    'fav_successful' => 'Add To Favourite Successfully !',
    'unfollow_successful' => 'Un-Followed Successfully !',
    'follow_successful' => 'Followed Successfully !',
    'deleted_successfully'=>'Deleted Successfully !',
    'saved_successfully'=>'Saved Successfully !',
    'wrong_data'=>'Wrong Data !',
    'offer_exists'=>'You cannot add more than one offer to the same object !',
    'payment_method_not_allowed'=>'Payment method not allowed !',
    'date_reserved'=>'Date Reserved !',
    'offer_expired'=>'Offer Expired !',
    'wrong_sequence'=>'Wrong Sequence !',
    'dont_have_permission'=>'Dont have permission !',

];
