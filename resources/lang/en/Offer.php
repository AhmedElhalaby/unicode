<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'offers' => 'Offers',
    'offer' => 'Offer',
    'the_offer' => 'The Offer',
    'brand_id' => 'Brand',
    'category_id' => 'Category',
    'description' => 'Description',
    'name' => 'Name',
    'expire_at' => 'Expire At',
    'website' => 'Website',
    'code' => 'Code',
    'barcode' => 'Barcode',
    'image' => 'Image',
    'reuse_hour' => 'Reuse Hours',
    'reuse_day' => 'Reuse Days',
    'show_count' => 'Show Count',

];
