@extends('admin.settings.main')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="{{ config('app.color') }}">
                    <h4 class="title">{{__('admin.edit')}} {{__(($TheName))}}</h4>
                </div>
                <div class="card-content">
                    <form action="{{url($redirect.'/'.$Object->id)}}" method="post" enctype="multipart/form-data">
                        <input name="_method" type="hidden" value="PUT">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label for="name" class="control-label">{{__('Setting.name')}} *</label>
                                    <input type="text" id="name" name="name" required class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{$Object->name}}">
                                </div>
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label for="name_ar" class="control-label">{{__('Setting.name_ar')}} *</label>
                                    <input type="text" id="name_ar" name="name_ar" required class="form-control {{ $errors->has('name_ar') ? ' is-invalid' : '' }}" value="{{$Object->name_ar}}">
                                </div>
                                @if ($errors->has('name_ar'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name_ar') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label for="text" class="control-label">{{__('Setting.text')}} *</label>
                                    <textarea type="text" id="text" name="text" required class="form-control {{ $errors->has('text') ? ' is-invalid' : '' }}">{{$Object->text}}</textarea>
                                </div>
                                @if ($errors->has('text'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('text') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label for="text_ar" class="control-label">{{__('Setting.text_ar')}} *</label>
                                    <textarea type="text" id="text_ar" name="text_ar" required class="form-control {{ $errors->has('text_ar') ? ' is-invalid' : '' }}">{{$Object->text_ar}}</textarea>
                                </div>
                                @if ($errors->has('text_ar'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('text_ar') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row submit-btn">
                            <button type="submit" class="btn btn-primary" style="margin-left:15px;margin-right:15px;">{{__('admin.save')}}</button>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
