@extends('admin.admins.main')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="{{ config('app.color') }}">
                    <h4 class="title">{{__('admin.edit')}} {{__(($TheName))}}</h4>
                </div>
                <div class="card-content">
                    <form action="{{url($redirect.'/'.$Object->id)}}" method="post" enctype="multipart/form-data">
                        <input name="_method" type="hidden" value="PUT">
                        @csrf
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label for="first_name" class="control-label">{{__('Models/Admin.first_name')}} *</label>
                                    <input type="text" id="first_name" name="first_name" required class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }}" value="{{$Object->first_name}}">
                                </div>
                                @if ($errors->has('first_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label for="second_name" class="control-label">{{__('Models/Admin.second_name')}} *</label>
                                    <input type="text" id="second_name" name="second_name" required class="form-control {{ $errors->has('second_name') ? ' is-invalid' : '' }}" value="{{$Object->second_name}}">
                                </div>
                                @if ($errors->has('second_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('second_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label for="third_name" class="control-label">{{__('Models/Admin.third_name')}} *</label>
                                    <input type="text" id="third_name" name="third_name" required class="form-control {{ $errors->has('third_name') ? ' is-invalid' : '' }}" value="{{$Object->third_name}}">
                                </div>
                                @if ($errors->has('third_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('third_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group label-floating">
                                    <label for="email" class="control-label">{{__('Models/Admin.email')}} *</label>
                                    <input type="email" id="email" name="email" required class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{$Object->email}}">
                                </div>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label for="identity_num" class="control-label">{{__('Models/Admin.identity_num')}} *</label>
                                    <input type="number" id="identity_num" name="identity_num" required class="form-control {{ $errors->has('identity_num') ? ' is-invalid' : '' }}" value="{{$Object->identity_num}}">
                                </div>
                                @if ($errors->has('identity_num'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('identity_num') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-3">
                                <div class="form-group label-floating">
                                    <label for="salary" class="control-label">{{__('Models/Admin.salary')}} *</label>
                                    <input type="number" id="salary" name="salary" class="form-control {{ $errors->has('salary') ? ' is-invalid' : '' }}" value="{{$Object->salary}}">
                                </div>
                                @if ($errors->has('salary'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('salary') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group label-floating">
                                    <label for="permission" class="control-label">{{__('Models/Admin.permission')}} *</label>
                                    <select id="permission" name="permission" required class="form-control {{ $errors->has('permission') ? ' is-invalid' : '' }}">
                                        @foreach(\App\Models\Admin::$Permission as $key => $value)
                                            <option value="{{$value}}" @if($Object->permission == $value) selected @endif>{{__('Models/Admin.Permission.'.$key)}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if ($errors->has('permission'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('permission') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-5">
                                <label for="cv" class="control-label">{{__('Models/Admin.cv')}} </label>
                                <input type="file" name="cv" style="" id="cv"  class="  {{ $errors->has('cv') ? ' is-invalid' : '' }}">
                                @if ($errors->has('cv'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('cv') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="col-md-2">
                                <a href="{{$Object->getCv()}}" download class="btn btn-primary btn-block"><i class="fa fa-download"></i></a>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4 {{ $errors->has('image') ? ' is-invalid' : '' }}">
                                <label for="image" class="control-label">{{__('Models/Admin.image')}} </label>
                                <input type="file" name="image" style="display: none" id="image"  class="form-control  {{ $errors->has('image') ? ' is-invalid' : '' }}" onchange="readURL(this,'image');">
                                <img id="blah_image" onclick="document.getElementById('image').click()" src="{{$Object->getImage()?:asset('public/assets/img/upload.png')}}" style="width: 120px;height: 120px" alt="upload image" class="thumbnail" />
                                @if ($errors->has('image'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-4">
                                <label for="identity" class="control-label">{{__('Models/Admin.identity')}}</label>
                                <input type="file" name="identity" style="display: none" id="identity"  class="form-control  {{ $errors->has('identity') ? ' is-invalid' : '' }}" onchange="readURL(this,'identity');">
                                <img id="blah_identity" onclick="document.getElementById('identity').click()" src="{{$Object->getIdentity()?:asset('public/assets/img/upload.png')}}" style="width: 120px;height: 120px" alt="upload image" class="thumbnail" />
                                @if ($errors->has('identity'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('identity') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-4">
                                <label for="passport" class="control-label">{{__('Models/Admin.passport')}}</label>
                                <input type="file" name="passport" style="display: none" id="passport"  class="form-control  {{ $errors->has('passport') ? ' is-invalid' : '' }}" onchange="readURL(this,'passport');">
                                <img id="blah_passport" onclick="document.getElementById('passport').click()" src="{{$Object->getPassport()?:asset('public/assets/img/upload.png')}}" style="width: 120px;height: 120px" alt="upload image" class="thumbnail" />
                                @if ($errors->has('passport'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('passport') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row submit-btn">
                            <button type="submit" class="btn btn-primary" style="margin-left:15px;margin-right:15px;">{{__('admin.save')}}</button>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script>
        function readURL(input,id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah_'+id)
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
