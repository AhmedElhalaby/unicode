@extends('admin.categories.main')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="{{ config('app.color') }}">
                <div class="row">
                    <div class="col-md-8">
                        <h4 class="title">{{__($Names)}}</h4>
                    </div>
                    <div class="col-md-4">
                        <div class="search" >
                            <form class="search form-group label-floating" action="{{url($redirect)}}">
                                <label for="q" class="control-label">{{__('admin.search')}} ...</label>
                                <input type="text" name="q" id="q" class="form-control" value="{{app('request')->input('q')}}">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-content table-responsive">
                <table class="table">
                    <thead class="text-primary">
                        <th></th>
                        <th>{{__('Brand.category_id')}} </th>
                        <th>{{__('Brand.name')}} </th>
                        <th><a href="#" onclick="AdvanceSearch()">{{__('admin.advance_search')}} <i id="advance_search_caret" class="fa fa-caret-down"></i></a></th>
                    </thead>
                    <tbody>
                        <tr id="advance_search">
                            <form action="{{url($redirect)}}" >
                                <td></td>
                                <td>
                                    <div class="form-group ">
                                        <label for="category_id" class="hidden"></label>
                                        <select id="category_id" name="category_id" required class="form-control {{ $errors->has('category_id') ? ' is-invalid' : '' }}" value="{{old('category_id')}}">
                                            @foreach(\App\Models\Category::all() as $category)
                                                <option value="{{$category->id}}" @if($category->id == app('request')->input('category_id')) selected @endif>{{$category->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group" style="margin:0;padding: 0 ">
                                        <label for="name" class="hidden"></label>
                                        <input type="text" name="name" style="margin: 0;padding: 0" id="name" placeholder="{{__('Brand.name')}}" class="form-control" value="{{app('request')->input('name')}}">
                                    </div>
                                </td>
                                <td>
                                    <input type="submit" class="btn btn-sm btn-primary" style="margin: 0;" value="{{__('admin.search')}}">
                                </td>
                            </form>
                        </tr>
                    @foreach($Objects as $item)
                    <tr>
                        <td><img src="{{$item->getImage()}}" alt="" style="width: 75px;height: 75px" class="thumbnail img-thumbnail"></td>
                        <td>{{($item->category)?$item->category->name:'-'}}</td>
                        <td>{{$item->name}}</td>
                        <td class="text-primary">
                            @if(auth('admin')->user()->permission == 3)
                            <a href="{{url($redirect.'/'.$item->id.'/edit')}}" data-toggle="tooltip" data-placement="bottom" title="{{__('admin.edit')}}" class="fs-20"><i class="fa fa-edit"></i></a>
                            <a href="#" class="fs-20" data-toggle="modal" data-target="#delete" onclick="document.getElementById('del_name').innerHTML = '{{$item->name}}';document.getElementById('id').value = '{{$item->id}}'"><i class="fa fa-trash" data-toggle="tooltip" data-placement="bottom" title="{{__('admin.delete')}}"></i></a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="pagination-div">
        {{ $Objects->appends(['q' => request()->q,'name' => request()->name,'category_id' => request()->category_id])->links() }}
    </div>
</div>
@endsection

