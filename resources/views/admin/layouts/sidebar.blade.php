<li class="nav-item @if(url()->current() == url('admin')) active @endif ">
    <a href="{{url('admin')}}" class="nav-link">
        <i class="material-icons">dashboard</i>
        <p>{{__('admin.sidebar.home')}}</p>
    </a>
</li>
@if(auth('admin')->user()->permission == 3)
<li class="nav-item ">
    <a class="nav-link collapsed" data-toggle="collapse" href="#app_managements" aria-expanded="false">
        <i class="material-icons">keyboard_arrow_down</i>
        <p> {{__('admin.sidebar.app_managements')}}</p>
    </a>
    <div class="collapse @if(strpos(url()->current() , url('admin/app_managements'))===0) in @endif" id="app_managements" @if(strpos(url()->current() , url('admin/app_managements'))===0) aria-expanded="true" @endif>
        <ul class="nav">
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_managements/admins'))===0) active @endif">
                <a href="{{url('admin/app_managements/admins')}}" class="nav-link">
                    <i class="material-icons">group_add</i>
                    <p>{{__('admin.sidebar.admins')}}</p>
                </a>
            </li>
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_managements/logs'))===0) active @endif">
                <a href="{{url('admin/app_managements/logs')}}" class="nav-link">
                    <i class="material-icons">view_headline</i>
                    <p>{{__('admin.sidebar.logs')}}</p>
                </a>
            </li>
        </ul>
    </div>
</li>
@endif
<li class="nav-item ">
    <a class="nav-link collapsed" data-toggle="collapse" href="#app_settings" aria-expanded="false">
        <i class="material-icons">keyboard_arrow_down</i>
        <p> {{__('admin.sidebar.app_settings')}}</p>
    </a>
    <div class="collapse @if(strpos(url()->current() , url('admin/app_settings'))===0) in @endif" id="app_settings" @if(strpos(url()->current() , url('admin/app_settings'))===0) aria-expanded="true" @endif>
        <ul class="nav">
            @if(auth('admin')->user()->permission > 1)
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_settings/settings'))===0) active @endif">
                <a href="{{url('admin/app_settings/settings')}}" class="nav-link">
                    <i class="material-icons">settings</i>
                    <p>{{__('admin.sidebar.settings')}}</p>
                </a>
            </li>
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_settings/universities'))===0) active @endif">
                <a href="{{url('admin/app_settings/universities')}}" class="nav-link">
                    <i class="material-icons">account_balance</i>
                    <p>{{__('admin.sidebar.universities')}}</p>
                </a>
            </li>
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_settings/categories'))===0) active @endif">
                <a href="{{url('admin/app_settings/categories')}}" class="nav-link">
                    <i class="material-icons">bar_chart</i>
                    <p>{{__('admin.sidebar.categories')}}</p>
                </a>
            </li>
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_settings/brands'))===0) active @endif">
                <a href="{{url('admin/app_settings/brands')}}" class="nav-link">
                    <i class="material-icons">card_giftcard</i>
                    <p>{{__('admin.sidebar.brands')}}</p>
                </a>
            </li>
            @endif
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_settings/offers'))===0) active @endif">
                <a href="{{url('admin/app_settings/offers')}}" class="nav-link">
                    <i class="material-icons">flash_on</i>
                    <p>{{__('admin.sidebar.offers')}}</p>
                </a>
            </li>
        </ul>
    </div>
</li>
@if(auth('admin')->user()->permission == 3)
<li class="nav-item ">
    <a class="nav-link collapsed" data-toggle="collapse" href="#app_users" aria-expanded="false">
        <i class="material-icons">keyboard_arrow_down</i>
        <p> {{__('admin.sidebar.app_users')}}</p>
    </a>
    <div class="collapse @if(strpos(url()->current() , url('admin/app_users'))===0) in @endif" id="app_users" @if(strpos(url()->current() , url('admin/app_users'))===0) aria-expanded="true" @endif>
        <ul class="nav">
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_users/users'))===0) active @endif">
                <a href="{{url('admin/app_users/users')}}" class="nav-link">
                    <i class="material-icons">group_add</i>
                    <p>{{__('admin.sidebar.users')}}</p>
                </a>
            </li>
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_users/logs'))===0) active @endif">
                <a href="{{url('admin/app_users/logs')}}" class="nav-link">
                    <i class="material-icons">view_headline</i>
                    <p>{{__('admin.sidebar.logs')}}</p>
                </a>
            </li>
        </ul>
    </div>
</li>
@endif

@if(auth('admin')->user()->permission == 3)
<li class="nav-item ">
    <a class="nav-link collapsed" data-toggle="collapse" href="#app_reports" aria-expanded="false">
        <i class="material-icons">keyboard_arrow_down</i>
        <p> {{__('admin.sidebar.app_reports')}}</p>
    </a>
    <div class="collapse @if(strpos(url()->current() , url('admin/app_reports'))===0) in @endif" id="app_reports" @if(strpos(url()->current() , url('admin/app_reports'))===0) aria-expanded="true" @endif>
        <ul class="nav">
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_reports/emp_reports'))===0) active @endif">
                <a href="{{url('admin/app_reports/emp_reports')}}" class="nav-link">
                    <i class="material-icons">list</i>
                    <p>{{__('admin.sidebar.emp_reports')}}</p>
                </a>
            </li>
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_reports/user_reports'))===0) active @endif">
                <a href="{{url('admin/app_reports/user_reports')}}" class="nav-link">
                    <i class="material-icons">bubble_chart</i>
                    <p>{{__('admin.sidebar.user_reports')}}</p>
                </a>
            </li>
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_reports/offer_reports'))===0) active @endif">
                <a href="{{url('admin/app_reports/offer_reports')}}" class="nav-link">
                    <i class="material-icons">flash_on</i>
                    <p>{{__('admin.sidebar.offer_reports')}}</p>
                </a>
            </li>
        </ul>
    </div>
</li>
@endif
