@extends('admin.offers.main')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="{{ config('app.color') }}">
                <div class="row">
                    <div class="col-md-8">
                        <h4 class="title">{{__($Names)}}</h4>
                    </div>
                    <div class="col-md-4">
                        <div class="search" >
                            <form class="search form-group label-floating" action="{{url($redirect)}}">
                                <label for="q" class="control-label">{{__('admin.search')}} ...</label>
                                <input type="text" name="q" id="q" class="form-control" value="{{app('request')->input('q')}}">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-content table-responsive">
                <table class="table">
                    <thead class="text-primary">
                        <th></th>
                        <th>{{__('Offer.brand_id')}} </th>
                        <th>{{__('Offer.category_id')}} </th>
                        <th>{{__('Offer.description')}} </th>
                        <th>{{__('Offer.expire_at')}} </th>
                        <th>{{__('Models/Admin.status')}} </th>
                        <th><a href="#" onclick="AdvanceSearch()">{{__('admin.advance_search')}} <i id="advance_search_caret" class="fa fa-caret-down"></i></a></th>
                    </thead>
                    <tbody>
                        <tr id="advance_search">
                            <form action="{{url($redirect)}}" >
                                <td></td>
                                <td>
                                    <div class="form-group" style="margin:0;padding: 0 ">
                                        <label for="brand_id" class="hidden"></label>
                                        <select name="brand_id" style="margin: 0;padding: 0" id="brand_id" class="form-control">
                                            @foreach(\App\Models\Brand::all() as $brand)
                                                <option value="{{$brand->id}}" @if($brand->id == app('request')->input('brand_id')) selected @endif>{{$brand->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group" style="margin:0;padding: 0 ">
                                        <label for="category_id" class="hidden"></label>
                                        <select name="category_id" style="margin: 0;padding: 0" id="category_id" class="form-control">
                                            @foreach(\App\Models\Category::all() as $category)
                                                <option value="{{$category->id}}" @if($category->id == app('request')->input('category_id')) selected @endif>{{$category->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group" style="margin:0;padding: 0 ">
                                        <label for="description" class="hidden"></label>
                                        <input type="text" name="description" style="margin: 0;padding: 0" id="description" placeholder="{{__('Offer.description')}}" class="form-control" value="{{app('request')->input('description')}}">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group" style="margin:0;padding: 0 ">
                                        <label for="expire_at" class="hidden"></label>
                                        <input type="date" name="expire_at" style="margin: 0;padding: 0" id="expire_at" placeholder="{{__('Offer.expire_at')}}" class="form-control" value="{{app('request')->input('expire_at')}}">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group" style="margin:0;padding: 0 ">
                                        <label for="is_active" class="hidden"></label>
                                        <select type="is_active" name="is_active" style="margin: 0;padding: 0" id="is_active" class="form-control">
                                            <option value=""@if(app('request')->has('is_active') && app('request')->input('is_active') == '') selected @endif>-</option>
                                            <option value="1" @if(app('request')->has('is_active') && app('request')->input('is_active') == '1') selected @endif>{{__('Models/Admin.active')}}</option>
                                            <option value="0" @if(app('request')->has('is_active') && app('request')->input('is_active') == '0') selected @endif>{{__('Models/Admin.in_active')}}</option>
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <input type="submit" class="btn btn-sm btn-primary" style="margin: 0;" value="{{__('admin.search')}}">
                                </td>
                            </form>
                        </tr>
                    @foreach($Objects as $item)
                    <tr>
                        <td><img src="{{$item->getImage()}}" alt="" style="width: 75px;height: 75px" class="thumbnail img-thumbnail"></td>
                        <td>{{($item->brand)?$item->brand->name:' - '}}</td>
                        <td>{{($item->brand)?$item->category->name:' -'}}</td>
                        <td>{{$item->description}}</td>
                        <td>{{$item->expire_at}}</td>
                        <td>@if($item->is_active) <span class="label label-success">{{__('Models/Admin.active')}}</span>@else  <span class="label label-danger">{{__('Models/Admin.in_active')}}</span> @endif</td>
                        <td class="text-primary">
                            <a href="{{url($redirect.'/'.$item->id.'/edit')}}" data-toggle="tooltip" data-placement="bottom" title="{{__('admin.edit')}}" class="fs-20"><i class="fa fa-edit"></i></a>
                            @if(auth('admin')->user()->permission == 3)
                                <a href="{{url($redirect.'/'.$item->id.'/activation')}}" data-toggle="tooltip" data-placement="bottom" title="@if($item->is_active) {{__('Models/Admin.do_in_active')}} @else {{__('Models/Admin.do_active')}} @endif" class="fs-20"><i class="fa @if($item->is_active) fa-window-close @else fa-check-square @endif"></i></a>
                                <a href="#" class="fs-20" data-toggle="modal" data-target="#delete" onclick="document.getElementById('del_name').innerHTML = '{{$item->name}}';document.getElementById('id').value = '{{$item->id}}'"><i class="fa fa-trash" data-toggle="tooltip" data-placement="bottom" title="{{__('admin.delete')}}"></i></a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="pagination-div">
        {{ $Objects->appends(['q' => request()->q,'name' => request()->name,'name_ar' => request()->name_ar,'is_active' => request()->is_active])->links() }}
    </div>
</div>
@endsection

