@extends('admin.offers.main')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="{{ config('app.color') }}">
                    <h4 class="title">{{__('admin.add')}} {{__($Name)}}</h4>
                </div>
                <div class="card-content">
                    <form action="{{url($redirect)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label for="category_id" class="control-label">{{__('Offer.category_id')}} *</label>
                                    <select id="category_id" name="category_id" required class="form-control {{ $errors->has('category_id') ? ' is-invalid' : '' }}">
                                        @foreach(\App\Models\Category::all() as $Category)
                                            <option value="{{$Category->id}}" @if(old('category_id') == $Category->id) selected @endif>{{$Category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if ($errors->has('category_id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('category_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label for="brand_id" class="control-label">{{__('Offer.brand_id')}} *</label>
                                    <select id="brand_id" name="brand_id" required class="form-control {{ $errors->has('brand_id') ? ' is-invalid' : '' }}">
                                        @foreach(\App\Models\Brand::all() as $brand)
                                            <option value="{{$brand->id}}" @if(old('brand_id') == $brand->id) selected @endif>{{$brand->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if ($errors->has('brand_id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('brand_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label for="name" class="control-label">{{__('Offer.name')}} *</label>
                                    <input type="text" id="name" name="name" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{old('name')}}">
                                </div>
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label for="description" class="control-label">{{__('Offer.description')}} *</label>
                                    <input type="text" id="description" name="description" class="form-control {{ $errors->has('description') ? ' is-invalid' : '' }}" value="{{old('description')}}">
                                </div>
                                @if ($errors->has('description'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group label-floating is-focused">
                                    <label for="expire_at" class="control-label">{{__('Offer.expire_at')}} *</label>
                                    <input type="date" id="expire_at" name="expire_at" class="form-control {{ $errors->has('expire_at') ? ' is-invalid' : '' }}" value="{{old('expire_at')}}">
                                </div>
                                @if ($errors->has('expire_at'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('expire_at') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-5">
                                <div class="form-group label-floating">
                                    <label for="website" class="control-label">{{__('Offer.website')}} *</label>
                                    <input type="url" id="website" name="website" class="form-control {{ $errors->has('website') ? ' is-invalid' : '' }}" value="{{old('website')}}">
                                </div>
                                @if ($errors->has('website'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('website') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-2">
                                <div class="form-group label-floating">
                                    <label for="reuse_hour" class="control-label">{{__('Offer.reuse_hour')}} *</label>
                                    <input type="number" id="reuse_hour" name="reuse_hour" class="form-control {{ $errors->has('reuse_hour') ? ' is-invalid' : '' }}" value="0">
                                </div>
                                @if ($errors->has('reuse_hour'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('reuse_hour') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-2">
                                <div class="form-group label-floating">
                                    <label for="reuse_day" class="control-label">{{__('Offer.reuse_day')}} *</label>
                                    <input type="number" id="reuse_day" name="reuse_day" class="form-control {{ $errors->has('reuse_day') ? ' is-invalid' : '' }}" value="0">
                                </div>
                                @if ($errors->has('reuse_day'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('reuse_day') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label for="code" class="control-label">{{__('Offer.code')}} *</label>
                                    <input type="text" id="code" name="code" class="form-control {{ $errors->has('code') ? ' is-invalid' : '' }}" value="{{old('code')}}">
                                </div>
                                @if ($errors->has('code'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('code') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label for="barcode" class="control-label">{{__('Offer.barcode')}} </label>
                                    <input type="text" id="barcode" name="barcode" class="form-control {{ $errors->has('barcode') ? ' is-invalid' : '' }}" value="{{old('barcode')}}">
                                </div>
                                @if ($errors->has('barcode'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('barcode') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label for="image" class="control-label">{{__('Offer.image')}}</label>
                                <input type="file" name="image" style="display: none" id="image"  class="form-control  {{ $errors->has('name') ? ' is-invalid' : '' }}" onchange="readURL(this);">
                                <img id="blah" onclick="document.getElementById('image').click()" src="{{asset('public/assets/img/upload.png')}}" style="width: 120px;height: 120px" alt="upload image" class="thumbnail" />
                                @if ($errors->has('image'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row submit-btn">
                            <button type="submit" class="btn btn-primary" style="margin-left:15px;margin-right:15px;">{{__('admin.save')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
