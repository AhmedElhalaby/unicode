@extends('admin.categories.main')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="{{ config('app.color') }}">
                <div class="row">
                    <div class="col-md-8">
                        <h4 class="title">{{__($Names)}}</h4>
                    </div>
                    <div class="col-md-4">
                        <div class="search" >
                            <form class="search form-group label-floating" action="{{url($redirect)}}">
                                <label for="q" class="control-label">{{__('admin.search')}} ...</label>
                                <input type="text" name="q" id="q" class="form-control" value="{{app('request')->input('q')}}">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-content table-responsive">
                <table class="table">
                    <thead class="text-primary">
                        <th>{{__('Category.name')}} </th>
                        <th>{{__('Category.name_ar')}}</th>
                        <th>{{__('Category.order')}}</th>
                        <th>{{__('Category.status')}}</th>
                        <th><a href="#" onclick="AdvanceSearch()">{{__('admin.advance_search')}} <i id="advance_search_caret" class="fa fa-caret-down"></i></a></th>
                    </thead>
                    <tbody>
                        <tr id="advance_search">
                            <form action="{{url($redirect)}}" >
                                <td>
                                    <div class="form-group" style="margin:0;padding: 0 ">
                                        <label for="name" class="hidden"></label>
                                        <input type="text" name="name" style="margin: 0;padding: 0" id="name" placeholder="{{__('Category.name')}}" class="form-control" value="{{app('request')->input('name')}}">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group" style="margin:0;padding: 0 ">
                                        <label for="name_ar" class="hidden"></label>
                                        <input type="text" name="name_ar" style="margin: 0;padding: 0" id="name_ar" placeholder="{{__('Category.name_ar')}}" class="form-control" value="{{app('request')->input('name_ar')}}">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group" style="margin:0;padding: 0 ">
                                        <label for="order" class="hidden"></label>
                                        <input type="number" name="order" style="margin: 0;padding: 0" id="order" placeholder="{{__('Category.order')}}" class="form-control" value="{{app('request')->input('order')}}">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group" style="margin:0;padding: 0 ">
                                        <label for="is_active" class="hidden"></label>
                                        <select type="is_active" name="is_active" style="margin: 0;padding: 0" id="is_active" class="form-control">
                                            <option value=""@if(app('request')->has('is_active') && app('request')->input('is_active') == '') selected @endif>-</option>
                                            <option value="1" @if(app('request')->has('is_active') && app('request')->input('is_active') == '1') selected @endif>{{__('Category.active')}}</option>
                                            <option value="0" @if(app('request')->has('is_active') && app('request')->input('is_active') == '0') selected @endif>{{__('Category.in_active')}}</option>
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <input type="submit" class="btn btn-sm btn-primary" style="margin: 0;" value="{{__('admin.search')}}">
                                </td>
                            </form>
                        </tr>
                    @foreach($Objects as $item)
                    <tr>
                        <td>{{$item->name}}</td>
                        <td>{{$item->name_ar}}</td>
                        <td>{{$item->order}}</td>
                        <td>@if($item->is_active) <span class="label label-success">{{__('Category.active')}}</span>@else  <span class="label label-danger">{{__('Category.in_active')}}</span> @endif</td>
                        <td class="text-primary">
                            @if(auth('admin')->user()->permission == 3)
                            <a href="{{url($redirect.'/'.$item->id.'/edit')}}" data-toggle="tooltip" data-placement="bottom" title="{{__('admin.edit')}}" class="fs-20"><i class="fa fa-edit"></i></a>
                            <a href="{{url($redirect.'/'.$item->id.'/activation')}}" data-toggle="tooltip" data-placement="bottom" title="@if($item->is_active) {{__('Category.do_in_active')}} @else {{__('Category.do_active')}} @endif" class="fs-20"><i class="fa @if($item->is_active) fa-window-close @else fa-check-square @endif"></i></a>
                            <a href="#" class="fs-20" data-toggle="modal" data-target="#delete" onclick="document.getElementById('del_name').innerHTML = '{{$item->name}}';document.getElementById('id').value = '{{$item->id}}'"><i class="fa fa-trash" data-toggle="tooltip" data-placement="bottom" title="{{__('admin.delete')}}"></i></a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="pagination-div">
        {{ $Objects->appends(['q' => request()->q,'name' => request()->name,'name_ar' => request()->name_ar,'is_active' => request()->is_active])->links() }}
    </div>
</div>
@endsection

