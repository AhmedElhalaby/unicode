@extends('admin.emp_reports.main')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="{{ config('app.color') }}">
                <div class="row">
                    <div class="col-md-8">
                        <h4 class="title">{{__($Names)}}</h4>
                    </div>
                    <div class="col-md-4">
                        <div class="search" >
                            <form class="search form-group label-floating" action="{{url($redirect)}}">
                                <label for="q" class="control-label">{{__('admin.search')}} ...</label>
                                <input type="text" name="q" id="q" class="form-control" value="{{app('request')->input('q')}}">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-content table-responsive">
                <table class="table">
                    <thead class="text-primary">
                        <th>{{__('Models/UserReport.user_id')}} </th>
                        <th>{{__('Models/User.first_name')}} </th>
                        <th>{{__('Models/User.second_name')}} </th>
                        <th>{{__('Models/User.mobile')}} </th>
                        <th>{{__('Models/User.email')}} </th>
                        <th>{{__('Models/User.university_id')}} </th>
                        <th>{{__('Models/UserReport.num_of_codes')}}</th>
                        <th>{{__('Models/UserReport.last_login')}}</th>
                        <th>{{__('Models/User.status')}}</th>
                        <th><a href="#" onclick="AdvanceSearch()">{{__('admin.advance_search')}} <i id="advance_search_caret" class="fa fa-caret-down"></i></a></th>
                    </thead>
                    <tbody>
                        <tr id="advance_search">
                            <form action="{{url($redirect)}}" >
                                <td></td>
                                <td>
                                    <div class="form-group" style="margin:0;padding: 0 ">
                                        <label for="first_name" class="hidden"></label>
                                        <input type="text" name="first_name" style="margin: 0;padding: 0" id="first_name" placeholder="{{__('Models/User.first_name')}}" class="form-control" value="{{app('request')->input('first_name')}}">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group" style="margin:0;padding: 0 ">
                                        <label for="second_name" class="hidden"></label>
                                        <input type="text" name="second_name" style="margin: 0;padding: 0" id="second_name" placeholder="{{__('Models/User.second_name')}}" class="form-control" value="{{app('request')->input('second_name')}}">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group" style="margin:0;padding: 0 ">
                                        <label for="mobile" class="hidden"></label>
                                        <input type="text" name="mobile" style="margin: 0;padding: 0" id="mobile" placeholder="{{__('Models/User.mobile')}}" class="form-control" value="{{app('request')->input('mobile')}}">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group" style="margin:0;padding: 0 ">
                                        <label for="email" class="hidden"></label>
                                        <input type="text" name="email" style="margin: 0;padding: 0" id="mobile" placeholder="{{__('Models/User.email')}}" class="form-control" value="{{app('request')->input('email')}}">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group" style="margin:0;padding: 0 ">
                                        <label for="university_id" class="hidden"></label>
                                        <select type="university_id" name="university_id" style="margin: 0;padding: 0" id="university_id" class="form-control">
                                            <option value=""@if(app('request')->has('university_id') && app('request')->input('university_id') == '') selected @endif>-</option>
                                            @foreach(\App\Models\University::all() as $univ)
                                            <option value="{{$univ->id}}" @if(app('request')->has('university_id') && app('request')->input('university_id') == $univ->id) selected @endif>{{(app()->getLocale()=='ar')?$univ->name_ar:$univ->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </td>
                                <td></td>
                                <td></td>
                                <td>
                                    <div class="form-group" style="margin:0;padding: 0 ">
                                        <label for="is_active" class="hidden"></label>
                                        <select type="is_active" name="is_active" style="margin: 0;padding: 0" id="is_active" class="form-control">
                                            <option value=""@if(app('request')->has('is_active') && app('request')->input('is_active') == '') selected @endif>-</option>
                                            <option value="1" @if(app('request')->has('is_active') && app('request')->input('is_active') == '1') selected @endif>{{__('Models/Admin.active')}}</option>
                                            <option value="0" @if(app('request')->has('is_active') && app('request')->input('is_active') == '0') selected @endif>{{__('Models/Admin.in_active')}}</option>
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <input type="submit" class="btn btn-sm btn-primary" style="margin: 0;" value="{{__('admin.search')}}">
                                </td>
                            </form>
                        </tr>
                    @foreach($Objects as $item)
                        <tr>
                            <td><a href="{{url('admin/app_users/users/'.$item->id.'/edit')}}">{{$item->id}}</a></td>
                            <td>{{$item->first_name}}</td>
                            <td>{{$item->second_name}}</td>
                            <td>{{$item->mobile}}</td>
                            <td><a href="mailto:{{$item->email}}">{{$item->email}}</a></td>
                            <td>{{($item->university)?((app()->getLocale()=='ar')?$item->university->name_ar:$item->university->name):'-'}}</td>
                            <td>{{$item->offers_count()}}</td>
                            <td>{{$item->last_login()}}</td>
                            <td>@if($item->is_active) <span class="label label-success">{{__('Models/Admin.active')}}</span>@else  <span class="label label-danger">{{__('Models/Admin.in_active')}}</span> @endif</td>
                            <td class="text-primary">
                                {{__('-')}}
                            </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="pagination-div">
        {{ $Objects->appends(['q' => request()->q,'first_name' => request()->first_name,'second_name' => request()->second_name,'email' => request()->email,'mobile' => request()->mobile,'university_id' => request()->university_id,'is_active' => request()->is_active])->links() }}
    </div>
</div>
@endsection

