@extends('admin.admins.main')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="{{ config('app.color') }}">
                <div class="row">
                    <div class="col-md-8">
                        <h4 class="title">{{__($Names)}}</h4>
                    </div>
                    <div class="col-md-4">
                        <div class="search" >
                            <form class="search form-group label-floating" action="{{url($redirect)}}">
                                <label for="q" class="control-label">{{__('admin.search')}} ...</label>
                                <input type="text" name="q" id="q" class="form-control" value="{{app('request')->input('q')}}">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-content table-responsive">
                <table class="table">
                    <thead class="text-primary">
                        <th>{{__('Models/Log.admin_id')}} </th>
                        <th>{{__('Models/Log.type')}}</th>
                        <th>{{__('Models/Log.created_at')}}</th>
                        <th><a href="#" onclick="AdvanceSearch()">{{__('admin.advance_search')}} <i id="advance_search_caret" class="fa fa-caret-down"></i></a></th>
                    </thead>
                    <tbody>
                        <tr id="advance_search">
                            <form action="{{url($redirect)}}" >
                                <td>
                                    <div class="form-group" style="margin:0;padding: 0 ">
                                        <label for="admin_id" class="hidden"></label>
                                        <select type="admin_id" name="admin_id" style="margin: 0;padding: 0" id="admin_id" class="form-control">
                                            <option value=""@if(app('request')->has('admin_id') && app('request')->input('admin_id') == '') selected @endif>-</option>
                                            @foreach(\App\Models\Admin::all() as $admin)
                                                <option value="{{$admin->id}}" @if(app('request')->has('admin_id') && app('request')->input('admin_id') == $admin->id) selected @endif>{{$admin->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group" style="margin:0;padding: 0 ">
                                        <label for="type" class="hidden"></label>
                                        <select type="type" name="type" style="margin: 0;padding: 0" id="type" class="form-control">
                                            <option value=""@if(app('request')->has('type') && app('request')->input('type') == '') selected @endif>-</option>
                                            @php
                                                $type['Login'] = \App\Models\Log::$Type['Login'];
                                                $type['Logout'] = \App\Models\Log::$Type['Logout'];
                                                $type['Create Offer'] = \App\Models\Log::$Type['Create Offer'];
                                                $type['Update Offer'] = \App\Models\Log::$Type['Update Offer'];
                                            @endphp
                                            @foreach($type as $key => $value)
                                                <option value="{{$value}}" @if(app('request')->has('type') && app('request')->input('type') == $value) selected @endif>{{__('Models/Log.Type.'.\App\Models\Log::$TypeString[$value])}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group" style="margin:0;padding: 0 ">
                                        <label for="created_at" class="hidden"></label>
                                        <input type="date" name="created_at" style="margin: 0;padding: 0" id="created_at" class="form-control" value="{{app('request')->input('created_at') }}">
                                    </div>
                                </td>
                                <td>
                                    <input type="submit" class="btn btn-sm btn-primary" style="margin: 0;" value="{{__('admin.search')}}">
                                </td>
                            </form>
                        </tr>
                    @foreach($Objects as $item)
                    <tr>
                        <td>{{($item->admin)?$item->admin->first_name .' '. $item->admin->second_name .' '. $item->admin->third_name:' - '}}</td>
                        <td>{{__('Models/Log.Type.'.\App\Models\Log::$TypeString[$item->type])}}</td>
                        <td>{{$item->created_at->format('Y-m-d H:i')}}</td>
                        <td class="text-primary">
                            @if($item->ref_id)
                                <a href="{{url('admin/app_settings/offers/'.$item->ref_id.'/edit')}}"><i class="fa fa-eye"></i></a>
                            @else
                                {{__('-')}}
                            @endif
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="pagination-div">
        {{ $Objects->appends(['q' => request()->q,'name' => request()->name,'email' => request()->email,'is_active' => request()->is_active])->links() }}
    </div>
</div>
@endsection

