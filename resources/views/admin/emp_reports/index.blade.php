@extends('admin.emp_reports.main')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="{{ config('app.color') }}">
                <div class="row">
                    <div class="col-md-8">
                        <h4 class="title">{{__($Names)}}</h4>
                    </div>
                    <div class="col-md-4">
                        <div class="search" >
                            <form class="search form-group label-floating" action="{{url($redirect)}}">
                                <label for="q" class="control-label">{{__('admin.search')}} ...</label>
                                <input type="text" name="q" id="q" class="form-control" value="{{app('request')->input('q')}}">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-content table-responsive">
                <table class="table">
                    <thead class="text-primary">
                        <th>{{__('Models/EmpReport.admin_id')}} </th>
                        <th>{{__('Models/EmpReport.num_of_codes')}}</th>
                        <th>{{__('Models/EmpReport.last_login')}}</th>
                        <th>{{__('Models/Admin.status')}}</th>
                        <th>{{__('Models/Admin.permission')}}</th>
                        <th><a href="#" onclick="AdvanceSearch()">{{__('admin.advance_search')}} <i id="advance_search_caret" class="fa fa-caret-down"></i></a></th>
                    </thead>
                    <tbody>
                        <tr id="advance_search">
                            <form action="{{url($redirect)}}" >
                                <td>
                                    <div class="form-group" style="margin:0;padding: 0 ">
                                        <label for="admin_id" class="hidden"></label>
                                        <select type="admin_id" name="admin_id" style="margin: 0;padding: 0" id="admin_id" class="form-control">
                                            <option value=""@if(app('request')->has('admin_id') && app('request')->input('admin_id') == '') selected @endif>-</option>
                                            @foreach(\App\Models\Admin::all() as $admin)
                                                <option value="{{$admin->id}}" @if(app('request')->has('admin_id') && app('request')->input('admin_id') == $admin->id) selected @endif>{{$admin->first_name. ' '. $admin->second_name. ' '. $admin->third_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td>
                                    <div class="form-group" style="margin:0;padding: 0 ">
                                        <label for="is_active" class="hidden"></label>
                                        <select type="is_active" name="is_active" style="margin: 0;padding: 0" id="is_active" class="form-control">
                                            <option value=""@if(app('request')->has('is_active') && app('request')->input('is_active') == '') selected @endif>-</option>
                                            <option value="1" @if(app('request')->has('is_active') && app('request')->input('is_active') == '1') selected @endif>{{__('Models/Admin.active')}}</option>
                                            <option value="0" @if(app('request')->has('is_active') && app('request')->input('is_active') == '0') selected @endif>{{__('Models/Admin.in_active')}}</option>
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group" style="margin:0;padding: 0 ">
                                        <label for="permission" class="hidden"></label>
                                        <select type="permission" name="permission" style="margin: 0;padding: 0" id="permission" class="form-control">
                                            <option value=""@if(app('request')->has('permission') && app('request')->input('permission') == '') selected @endif>-</option>
                                            @foreach(\App\Models\Admin::$Permission as $key => $value)
                                                <option value="{{$value}}" @if(app('request')->has('permission') && app('request')->input('permission') == $value) selected @endif>{{__('Models/Admin.Permission.'.$key)}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <input type="submit" class="btn btn-sm btn-primary" style="margin: 0;" value="{{__('admin.search')}}">
                                </td>
                            </form>
                        </tr>
                    @foreach($Objects as $item)
                    <tr>
                        <td><a href="{{url('admin/app_managements/admins/'.$item->id.'/edit')}}">{{$item->id}}</a></td>
                        <td><a href="{{url('admin/app_settings/offers?admin_id='.$item->id)}}">{{$item->offers_count()}}</a></td>
                        <td>{{$item->last_login()}}</td>
                        <td>@if($item->is_active) <span class="label label-success">{{__('Models/Admin.active')}}</span>@else  <span class="label label-danger">{{__('Models/Admin.in_active')}}</span> @endif</td>
                        <td>{{__('Models/Admin.Permission.'.\App\Models\Admin::$PermissionString[$item->permission])}}</td>
                        <td class="text-primary">
                            {{__('-')}}
                        </td>

                    </tr>

                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="pagination-div">
        {{ $Objects->appends(['q' => request()->q,'name' => request()->name,'email' => request()->email,'is_active' => request()->is_active])->links() }}
    </div>
</div>
@endsection

