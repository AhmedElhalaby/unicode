@extends('admin.layouts.app')
@section('style')
@endsection
@section('head-icon')
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="display: inline-block" aria-expanded="false">
        <i class="material-icons text-primary" data-toggle="tooltip" data-placement="bottom" title="{{__('admin.export')}}" style="font-size: 30px">cloud_download</i>
    </a>
    <ul class="dropdown-menu" role="menu" style="min-width: auto;">
{{--        <li><a class="dropdown-item"  href="{{ url($redirect.'/option/export') }}?t=xls" data-toggle="tooltip" data-placement="bottom" title="{{__('admin.excel')}}"><i class="material-icons">table_chart</i> </a></li>--}}
{{--        <li><a class="dropdown-item" href="{{ url($redirect.'/option/export') }}?t=pdf" data-toggle="tooltip" data-placement="bottom" title="{{__('admin.pdf')}}"><i class="material-icons">picture_as_pdf</i> </a></li>--}}
        <li><a class="dropdown-item" target="_blank" href="{{ url($redirect.'/option/export') }}?t=print" data-toggle="tooltip" data-placement="bottom" title="{{__('admin.print')}}"><i class="material-icons">print</i> </a></li>
    </ul>
@endsection
@section('out-content')

@endsection
@section('script')
    <script>
        function AdvanceSearch(){
            let x =$( "#advance_search" );
            let y =$( "#advance_search_caret" );
            x.fadeToggle();
            if(x.is(":visible")){
                y.removeClass('fa-caret-down');
                y.addClass('fa-caret-up');
            }else {
                y.removeClass('fa-caret-up');
                y.addClass('fa-caret-down');
            }
        }
        @if(app('request')->has('active') || app('request')->has('name') || app('request')->has('email'))
            $( "#advance_search" ).fadeToggle();
        @endif
    </script>
@endsection
