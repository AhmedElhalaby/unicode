@extends('admin.offer_reports.main')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="{{ config('app.color') }}">
                <div class="row">
                    <div class="col-md-8">
                        <h4 class="title">{{__($Names)}}</h4>
                    </div>
                    <div class="col-md-4">
                        <div class="search" >
                            <form class="search form-group label-floating" action="{{url($redirect)}}">
                                <label for="q" class="control-label">{{__('admin.search')}} ...</label>
                                <input type="text" name="q" id="q" class="form-control" value="{{app('request')->input('q')}}">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-content table-responsive">
                <table class="table">
                    <thead class="text-primary">
                        <th></th>
                        <th>{{__('Offer.description')}} </th>
                        <th>{{__('Offer.show_count')}} </th>
                    </thead>
                    <tbody>
                        <tr id="advance_search">
                            <form action="{{url($redirect)}}" >
                                <td></td>
                                <td></td>
                                <td></td>
                            </form>
                        </tr>
                    @foreach($Objects as $Object)
                    <tr>
                        <td><img src="{{$Object->getImage()}}" alt="" style="width: 75px;height: 75px" class="thumbnail img-thumbnail"></td>
                        <td>{{$Object->description}}</td>
                        <td>{{\App\Models\Log::where('type',\App\Models\Log::$Type['Show Offer'])->where('ref_id',$Object->id)->count()}}</td>
                    </tr>

                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="pagination-div">
        {{ $Objects->links() }}
    </div>
</div>
@endsection

