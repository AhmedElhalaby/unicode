@extends('admin.users.main')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="{{ config('app.color') }}">
                    <h4 class="title">{{__('admin.add')}} {{__($Name)}}</h4>
                </div>
                <div class="card-content">
                    <form action="{{url($redirect)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label for="first_name" class="control-label">{{__('Models/User.first_name')}} *</label>
                                    <input type="text" id="first_name" name="first_name" required class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }}" value="{{old('first_name')}}">
                                </div>
                                @if ($errors->has('first_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label for="second_name" class="control-label">{{__('Models/User.second_name')}} *</label>
                                    <input type="text" id="second_name" name="second_name" required class="form-control {{ $errors->has('second_name') ? ' is-invalid' : '' }}" value="{{old('second_name')}}">
                                </div>
                                @if ($errors->has('second_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('second_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label for="email" class="control-label">{{__('Models/User.email')}} *</label>
                                    <input type="email" id="email" name="email" required class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{old('email')}}">
                                </div>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label for="mobile" class="control-label">{{__('Models/User.mobile')}} *</label>
                                    <input type="tel" id="mobile" name="mobile" required class="form-control {{ $errors->has('mobile') ? ' is-invalid' : '' }}" value="{{old('mobile')}}">
                                </div>
                                @if ($errors->has('mobile'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label for="university_id" class="control-label">{{__('Models/User.university_id')}} *</label>
                                    <select id="university_id" name="university_id" required class="form-control {{ $errors->has('university_id') ? ' is-invalid' : '' }}">
                                        @foreach(\App\Models\University::all() as $University)
                                            <option value="{{$University->id}}" @if($University->id == old('university_id')) selected @endif>{{(app()->getLocale()=='ar')?$University->name_ar:$University->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if ($errors->has('university_id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('university_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label for="collage_student_number" class="control-label">{{__('Models/User.collage_student_number')}} *</label>
                                    <input type="text" id="collage_student_number" name="collage_student_number" required class="form-control {{ $errors->has('collage_student_number') ? ' is-invalid' : '' }}" value="{{old('collage_student_number')}}">
                                </div>
                                @if ($errors->has('collage_student_number'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('collage_student_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label for="collage_student_email" class="control-label">{{__('Models/User.collage_student_email')}} *</label>
                                    <input type="email" id="collage_student_email" name="collage_student_email" required class="form-control {{ $errors->has('collage_student_email') ? ' is-invalid' : '' }}" value="{{old('collage_student_email')}}">
                                </div>
                                @if ($errors->has('collage_student_email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('collage_student_email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label for="collage_graduation_years" class="control-label">{{__('Models/User.collage_graduation_years')}} *</label>
                                    <input type="number" id="collage_graduation_years" name="collage_graduation_years" required class="form-control {{ $errors->has('collage_graduation_years') ? ' is-invalid' : '' }}" value="{{old('collage_graduation_years')}}">
                                </div>
                                @if ($errors->has('collage_graduation_years'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('collage_graduation_years') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label for="password" class="control-label">{{__('Models/User.password')}}  *</label>
                                    <input type="password" id="password" name="password" required class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" >
                                </div>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label for="password_confirmation" class="control-label">{{__('Models/User.password_confirmation')}} *</label>
                                    <input type="password" id="password_confirmation" name="password_confirmation" required class="form-control {{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" >
                                </div>
                                @if ($errors->has('password_confirmation'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row submit-btn">
                            <button type="submit" class="btn btn-primary" style="margin-left:15px;margin-right:15px;">{{__('admin.save')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

