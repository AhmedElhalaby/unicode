<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name');
            $table->string('second_name');
            $table->string('third_name');
            $table->string('identity_num')->unique();
            $table->string('email')->unique();
            $table->string('image')->nullable();
            $table->string('cv')->nullable();
            $table->string('identity')->nullable();
            $table->string('passport')->nullable();
            $table->string('password')->nullable();
            $table->double('salary')->nullable();
            $table->tinyInteger('permission')->default(1);
            $table->boolean('is_active')->default(true);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
